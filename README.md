# PyRetina - System identification

This repository contains tools for fitting models that describe how the retinal ganglion cells (retina's neurons) responds to visual stimuli.

## Installation
Clone this repository
```
$ git clone https://gitlab.com/balefebvre/pyretina_systemidentification.git
$ cd pyretina_systemidentification
```
create a virtual environment
```
$ virtualenv --python=python3.6 ~/.virtualenvs/pyretsysident
```
install all the required Python packages
```
$ pip install tensorflow-gpu==2.1.0  # or pip install tensorflow-cpu==2.1.0
$ pip install matplotlib==3.1.3
```
install the Python package provided by this repository
```
$ pip install .  # or pip install --editable .
```
