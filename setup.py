from setuptools import setup


packages = [
    'pyretina_systemidentification',
]
install_requires = [
    'matplotlib',
    'numpy',
    'pandas',
    'tensorboard',
    # 'tensorflow-cpu==2.1.0',
    # 'tensorflow-gpu==2.1.0',
]

setup(
    name='pyretina_systemidentification',
    version='0.0.0',
    packages=packages,
    install_requires=install_requires,
)
