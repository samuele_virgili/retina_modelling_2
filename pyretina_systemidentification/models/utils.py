import tensorflow as tf


class CorePlusReadoutModel(tf.keras.Model):
    """Model."""

    def __init__(self, core, readout, name='core_plus_readout_model', **kwargs):
        """Initialization of the model."""
        super().__init__(name=name, **kwargs)
        # ...
        self.core = core
        self.readout = readout

    def call(self, inputs, training=False, **kwargs):
        """Forward computation of the model."""
        internals = self.core(inputs, training=training)
        outputs = self.readout(internals)
        return outputs
