import copy
import logging
import matplotlib.patches as pcs
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import tensorflow as tf

from pyretina_systemidentification.activations.parametric_softplus import ParametricSoftplusActivation
from pyretina_systemidentification.regularizers.l1_smooth2d import L1Smooth2DRegularizer
from pyretina_systemidentification.initializers.sta import STATruncatedNormalInitializer
from pyretina_systemidentification.callbacks.reduce_learning_rate_on_plateau \
    import CustomReduceLearningRateOnPlateauCallback
from pyretina_systemidentification.callbacks.model_checkpoint import CustomModelCheckpointCallback
from pyretina_systemidentification.utils import corrcoef
from pyretina_systemidentification.utils import plot_scale_bar


logger = logging.getLogger(__name__)


# Model.

class LNModel(tf.keras.Model):
    """LN model."""

    default_filter_kwargs = {
        'kernel_size': None,
        'kernel_initializer': 'truncated_normal',
        'spike_triggered_average': None,
        'smoothness_factor': None,
        'sparsity_factor': None,
        'name': 'filter',
    }

    def __init__(self, filter_kwargs=None, learning_rate=0.002, name="ln_model", **kwargs):
        """Initialization of the model."""

        super().__init__(name=name, **kwargs)

        # Filter keyword arguments.
        self.filter_kwargs = copy.deepcopy(self.default_filter_kwargs)
        if filter_kwargs is not None:
            self.filter_kwargs.update(filter_kwargs)

        self.learning_rate = learning_rate  # TODO check default value!

        self.losses_map = dict()

        # Add filter layer (i.e. convolutional layer which acts like a dense layer).
        filters = 1  # i.e. dimensionality of the output space
        kernel_size = self.filter_kwargs['kernel_size']
        # i.e. specification of the height and width of the 2D convolutional window
        kernel_initializer = self.filter_kwargs['kernel_initializer']
        if isinstance(kernel_initializer, dict):
            assert 'name' in kernel_initializer, kernel_initializer
            kernel_initializer = copy.deepcopy(kernel_initializer)
            kernel_initializer_name = kernel_initializer.pop('name')
            kernel_initializer_kwargs = kernel_initializer
            if kernel_initializer_name == 'truncated normal':
                kernel_initializer = tf.keras.initializers.TruncatedNormal(
                    # mean=0.0,
                    # stddev=0.05,
                    # seed=None
                    **kernel_initializer_kwargs,
                )
            elif kernel_initializer_name == 'STA + truncated normal':
                assert 'sta' in kernel_initializer_kwargs, kernel_initializer_kwargs
                kernel_initializer = STATruncatedNormalInitializer(
                    # sta=None,
                    # stddev=0.05,
                    **kernel_initializer_kwargs,
                )
            else:
                raise ValueError("unexpected value: {}".format(kernel_initializer_name))
        kernel_regularizer = L1Smooth2DRegularizer(
            sparsity_factor=self.filter_kwargs['sparsity_factor'],
            smoothness_factor=self.filter_kwargs['smoothness_factor'],
        )
        # kernel_constraint = tf.keras.constraints.UnitNorm(axis=[0, 1, 2])  # n.b. bad idea
        kernel_constraint = tf.keras.constraints.MaxNorm(max_value=1.0, axis=[0, 1, 2])  # c.f. beta (activation weight)
        filter_kwargs = {
            'name': self.filter_kwargs['name'],
        }
        self.filter = tf.keras.layers.Convolution2D(
            filters,
            kernel_size,
            # strides=(1, 1),
            # padding='valid',
            # data_format=None,
            # dilation_rate=(1, 1),
            # activation=None,
            use_bias=False,  # c.f. gamma (activation weight)
            kernel_initializer=kernel_initializer,
            # bias_initializer='zeros',
            kernel_regularizer=kernel_regularizer,
            # bias_regularizer=None,
            # activity_regularizer=None,
            kernel_constraint=kernel_constraint,
            # bias_constraint=None,
            **filter_kwargs,
        )

        # Add flatten layer.
        flatten_kwargs = {
            'name': "flatten",
        }
        self.flatten = tf.keras.layers.Flatten(
            # data_format=None,
            **flatten_kwargs,
        )

        # Add activation layer.
        activation_kwargs = {
            # 'trainable': True,
            # 'name': 'parametric_softplus',
        }
        self.activation = ParametricSoftplusActivation(
            # threshold=20.0,
            minimum=1.0e-8,  # i.e. help learning with Poisson loss  # TODO correct value?
            **activation_kwargs
        )

    def compile(self, **kwargs):
        """Configure the learning process of the model."""

        if self._is_compiled:
            logger.warning("Model has already been compiles.")
        else:
            # optimizer_kwargs = dict()
            optimizer = tf.keras.optimizers.Adam(
                learning_rate=self.learning_rate,
                # beta_1=0.9,
                # beta_2=0.999,
                # epsilon=1e-07,
                # amsgrad=False,
                name='Adam',
                # **optimizer_kwargs
            )
            loss = tf.keras.losses.Poisson(
                # reduction=losses_utils.ReductionV2.AUTO,
                name='poisson'
            )
            metrics = [
                tf.keras.metrics.Poisson(
                    name='poisson',
                    # dtype=None
                )
            ]
            # compilation_kwargs = dict()
            super().compile(
                optimizer=optimizer,
                loss=loss,
                metrics=metrics,
                # loss_weights=None,
                # sample_weight_mode=None,
                # weighted_metrics=None,
                # target_tensors=None,
                # distribute=None,
                # **compilation_kwargs
            )

        return

    def call(self, inputs, **kwargs):
        """Forward computation of the model."""

        internals = inputs

        # Apply filter.
        internals = self.filter(internals)
        # # Register loss associated to the filter.
        assert len(self.filter.losses) == 1, self.filter.losses
        loss_name = self.filter.kernel.name
        loss_name = loss_name[loss_name.find('/') + 1:]  # i.e. remove model name
        loss_name = loss_name[:loss_name.find(':')]  # i.e. remove trailing index
        loss_name = "regularization/{}".format(loss_name)  # i.e. add prefix
        loss_value = self.filter.losses[0]  # TODO correct?
        self.losses_map[loss_name] = loss_value
        # Apply flatten.
        internals = self.flatten(internals)
        # Apply activation.
        internals = self.activation(internals)

        outputs = internals

        # Add one metric for each loss.
        logger.debug("self.losses: {}".format(self.losses))
        logger.debug("self.losses_map: {}".format(self.losses_map))
        for loss_name, loss_value in self.losses_map.items():
            self.add_metric(loss_value, aggregation='mean', name=loss_name)

        return outputs

    def create_handler(self, *args, **kwargs):

        return _LNModelHandler(self, *args, **kwargs)


create = LNModel  # i.e. alias


# Model handler.

class _LNModelHandler:

    def __init__(self, model, directory=None, train_data=None, val_data=None, test_data=None, name=None):

        model.compile()

        self._model = model
        self._directory = directory
        self._train_data = train_data
        self._val_data = val_data
        self._test_data = test_data
        self._name = name

        self._checkpoints = dict()
        self._run_name = None

    def _get_checkpoint_weights_path(self, tag='final', run_name=None):
        """Get checkpoint weights path."""

        if self._directory is not None:
            # .Resolve directory.
            if run_name is not None:
                directory = os.path.join(self._directory, run_name)
            else:
                directory = self._directory
            # Resolve path.
            if isinstance(tag, str):
                path = os.path.join(directory, "checkpoint_{}".format(tag))
            elif isinstance(tag, int):
                path = os.path.join(directory, "checkpoint_{:05d}".format(tag))
            else:
                raise TypeError("unexpected tag type: {}".format(type(tag)))
        else:
            path = None

        return path

    def train(self, train_data, val_data, epochs=None, is_gpu_available=False, run_name=None):
        """Train model."""

        train_x, train_y = train_data
        val_x, val_y = val_data

        # Save initial weights (if necessary).
        path = self._get_checkpoint_weights_path(tag='initial', run_name=run_name)
        if path is not None:
            self._model.save_weights(
                path,
                # overwrite=True,
                save_format='tf',  # or 'h5'?
            )

        # Infer/fit/train the model (with Numpy arrays).
        # # Prepare callbacks.
        monitor = 'val_poisson'
        callbacks = []
        # # # Enable checkpoints.
        if self._directory is not None:
            if run_name is None:  # TODO simplify!
                checkpoint_path = os.path.join(self._directory, "checkpoint_{epoch:05d}")
            else:
                checkpoint_path = os.path.join(self._directory, run_name, "checkpoint_{epoch:05d}")
            callback = CustomModelCheckpointCallback(
                checkpoint_path,
                monitor=monitor,
                verbose=1,  # {0 (quiet, default), 1 (update messages), 2 (update and debug messages)}
                save_best_only=True,  # False (default)
                save_weights_only=True,  # False (default)
                save_max=2,  # 10 max.
                # mode='auto',  # {'auto', 'min', 'max'}
                # save_freq='epoch',
                # **kwargs,
            )
            callbacks.append(callback)
        # # # Enable TensorBoard and image summaries.
        if self._directory is not None:
            if run_name is None:  # TODO simplify!
                tensorboard_path = os.path.join(self._directory, "logs")
            else:
                tensorboard_path = os.path.join(self._directory, "logs", run_name)
            # Enable TensorBoard.
            callback = tf.keras.callbacks.TensorBoard(
                log_dir=tensorboard_path,
                histogram_freq=1,  # in epochs  {0 (no computation, default), <integer> (computation)}
                write_graph=True,
                # write_images=False,
                # update_freq='epoch',  # {'batch', 'epoch' (default), <integer>}
                #                       # note that writing too frequently to TensorBoard can slow down the training
                profile_batch=0,  # {0 (disable), 2 (default), <integer>}
                # embeddings_freq=0,  # in epochs  {0 (no visualized), <integer> (visualized)}
                # embeddings_metadata=None,
                # **kwargs,
            )
            callbacks.append(callback)
            # Enable image summaries.
            import io
            def plot_to_image(figure):  # noqa
                """Converts the matplotlib plot specified by 'figure' to a PNG image and
                returns it. The supplied figure is closed and inaccessible after this call."""
                # Save the plot to a PNG in memory.
                buf = io.BytesIO()
                plt.savefig(buf, format='png')
                # Closing the figure prevents it from being displayed directly inside
                # the notebook.
                plt.close(figure)
                buf.seek(0)
                # Convert PNG buffer to TF image
                image = tf.image.decode_png(buf.getvalue(), channels=4)
                # Add the batch dimension
                image = tf.expand_dims(image, 0)
                return image

            def prepare_kernel_plot(kernel):  # noqa
                _, _, nb_features, nb_kernels = kernel.shape
                assert nb_features == 1, nb_features
                assert nb_kernels == 1, nb_kernels
                fig, axes = plt.subplots(
                    nrows=nb_features, ncols=nb_kernels, squeeze=False, figsize=(2.5 * 1.6, 2.5 * 1.6)
                )
                imshow_kwargs = {
                    'cmap': 'RdBu_r',
                    'vmin': -tf.math.reduce_max(tf.math.abs(kernel)),
                    'vmax': tf.math.reduce_max(tf.math.abs(kernel)),
                }
                kernel = kernel[:, :, 0, 0]
                ax = axes[0, 0]
                ax.imshow(kernel, **imshow_kwargs)
                # ax.xaxis.set_major_locator(plt.MaxNLocator(2))
                # ax.yaxis.set_major_locator(plt.MaxNLocator(2))
                ax.xaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[1] - 1]))
                ax.yaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[0] - 1]))
                ax.set_xlabel("x")
                ax.set_ylabel("y")
                fig.tight_layout()
                return fig

            def prepare_activation_plot(activation):  # noqa
                x_min = -5.0
                x_max = +5.0
                x = np.linspace(x_min, x_max, num=int(10.0 * (x_max - x_min)), dtype=np.float32)
                y = activation(x)
                fig, ax = plt.subplots(figsize=(2.5 * 1.6, 2.5 * 1.6))
                ax.set_aspect('equal')
                ax.plot(x, y, color='black')
                assert tf.size(activation.alpha) == 1, tf.shape(activation)
                # alpha = tf.reshape(activation.alpha, [])  # i.e. reshape to scalar
                # beta = tf.reshape(activation.beta, [])  # i.e. reshape to scalar
                # gamma = tf.reshape(activation.gamma, [])  # i.e. reshape to scalar
                alpha = activation.alpha.numpy().ravel()[0]
                beta = activation.beta.numpy().ravel()[0]
                gamma = activation.gamma.numpy().ravel()[0]
                ax.axvline(x=-1.0, color='grey', linewidth=0.3)
                ax.axvline(x=0.0, color='grey', linewidth=0.3)
                ax.axvline(x=+1.0, color='grey', linewidth=0.3)
                ax.axhline(y=np.log(2.0), color='grey', linewidth=0.3)
                ax.axvline(x=(-1.0 - gamma) / beta, color='black', linestyle='dashed')
                ax.axvline(x=(0.0 - gamma) / beta, color='black', linestyle='dashed')
                ax.axvline(x=(+1.0 - gamma) / beta, color='black', linestyle='dashed')
                ax.axhline(y=alpha * np.log(2.0), color='black', linestyle='dashed')
                ax.set_xlim(x_min, x_max)
                ax.set_ylim(bottom=0.0)
                # ax.xaxis.set_major_locator(plt.MaxNLocator(3))
                # ax.yaxis.set_major_locator(plt.MaxNLocator(3))
                ax.xaxis.set_major_locator(plt.FixedLocator([-1.0, 0.0, +1.0]))
                ax.yaxis.set_major_locator(plt.FixedLocator([0.0, 1.0]))
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.set_xlabel("x")
                ax.set_ylabel("y")
                fig.tight_layout()
                return fig

            # image_summaries_path = os.path.join(tensorboard_path, "train", "plugins", "images")
            image_summaries_path = os.path.join(tensorboard_path, "train")
            image_summaries_writer = tf.summary.create_file_writer(image_summaries_path)

            def log_image_summaries(epoch):  # noqa
                if epoch is None or epoch < 10 - 1:  # i.e. the maximum number of images visible in tensorboard in 10.
                    with image_summaries_writer.as_default():
                        step = epoch if epoch is not None else 10000
                        # Log filter kernel.
                        filter_ = self._model.filter
                        kernel = filter_.kernel
                        summary_name = kernel.name[0:kernel.name.rfind('/')]
                        # i.e. remove weight name (keep layer name with scope)
                        tf.summary.image(
                            summary_name,
                            plot_to_image(prepare_kernel_plot(kernel)),
                            step=step,
                        )
                        # Log activation function.
                        activation = self._model.activation
                        alpha = activation.alpha
                        summary_name = alpha.name[0:alpha.name.rfind('/')]
                        # i.e. remove weight name (keep layer name with scope)
                        tf.summary.image(
                            summary_name,
                            plot_to_image(prepare_activation_plot(activation)),
                            step=step,
                        )
                        # TODO log predictions?
                return

            callback = tf.keras.callbacks.LambdaCallback(
                on_epoch_end=lambda epoch, logs: log_image_summaries(epoch),
                on_train_end=lambda logs: log_image_summaries(None),
            )
            callbacks.append(callback)
        # # # Enable learning rate decays.
        # learning_rate_decay_factor = 0.25
        learning_rate_decay_factor = 0.1  # (i.e. default)
        # TODO set a `minimum_learning_rate` to limit the number of learning rate decays?
        callback = CustomReduceLearningRateOnPlateauCallback(
            monitor=monitor,
            factor=learning_rate_decay_factor,
            patience=10,
            # patience=7,
            # patience=5,
            verbose=1,  # {0 (quiet, default), 1 (update messages)}
            # mode='auto',  # {auto (default), min, max}
            # min_delta=0.0001,  # threshold for measuring the new optimum, to only focus on significant changes
            # cooldown=0,  # number of epochs to wait before resuming normal operation after the learning rate has been reduced
            # min_lr=0,  # lower bound on the learning rate (default: 0)
            restore_best_weights=True,
            # **kwargs,
        )
        callbacks.append(callback)
        # # # Enable early stopping.
        callback = tf.keras.callbacks.EarlyStopping(
            monitor=monitor,
            # min_delta=0,
            # patience=15,  # use `0` instead (default)?
            patience=30,
            verbose=1,  # {0 (quiet?, default), 1 (update messages?)}
            # mode='auto',
            # baseline=None,
            restore_best_weights=True,
        )
        callbacks.append(callback)
        # # Run the inference of the model.
        # batch_size = 16
        batch_size = 32
        # batch_size = 64
        # batch_size = 128
        # batch_size = 256
        if epochs is None:
            epochs = 1000 if is_gpu_available else 100
        verbose = 2 if is_gpu_available else 1  # verbosity mode (0 = silent, 1 = progress bar (interactive environment), 2 = one line per epoch (production environment))
        history = self._model.fit(
            train_x,
            train_y,
            batch_size=batch_size,
            epochs=epochs,
            verbose=verbose,
            callbacks=callbacks,
            # validation_split=0.0,
            validation_data=(val_x, val_y),
            # shuffle=True,
            # class_weight=None,
            # sample_weight=None,
            # initial_epoch=0,  # useful for resuming a previous training session
            # steps_per_epoch=None,
            # validation_steps=None,
            # validation_freq=1,
            # max_queue_size=10,
            # workers=1,
            # use_multiprocessing=False,
            # **kwargs
        )

        # Save final weights (if necessary).
        path = self._get_checkpoint_weights_path(tag='final', run_name=run_name)
        if path is not None:
            self._model.save_weights(
                path,
                # overwrite=True,
                save_format='tf',  # or 'h5'?
            )

        return history

    def _convert_domain(self, domain):

        from tensorboard.plugins.hparams import api as hp  # TODO move to top of file?

        if domain is None:
            values = ['None']
            dtype = None  # TODO correct or remove!
            converted_domain = hp.Discrete(values, dtype=dtype)
        elif isinstance(domain, (int, float, str)):
            values = [domain]
            dtype = None  # TODO correct of remove!
            converted_domain = hp.Discrete(values, dtype=dtype)
        elif isinstance(domain, tuple):
            converted_domain = tuple([
                self._convert_domain(sub_domain)
                for sub_domain in domain
            ])  # TODO avoid `ValueError: not a domain: (RealInterval(0.002, 0.04), RealInterval(0.002, 0.04))`!
        elif isinstance(domain, set):
            values = list(domain)
            dtype = None  # TODO correct or remove!
            converted_domain = hp.Discrete(values, dtype=dtype)
        elif isinstance(domain, list):
            assert len(domain) == 2, domain
            min_value = domain[0]
            max_value = domain[1]
            if isinstance(min_value, float) and isinstance(max_value, float):
                assert min_value <= max_value, domain
                converted_domain = hp.RealInterval(min_value=min_value, max_value=max_value)
            elif isinstance(min_value, int) and isinstance(max_value, int):
                assert min_value <= max_value, domain
                converted_domain = hp.IntInterval(min_value=min_value, max_value=max_value)
            else:
                raise TypeError(
                    "unexpected min_value ({}) and max_value types({})".format(type(min_value), type(max_value))
                )
        else:
            # TODO correct!
            raise TypeError("unexpected domain type ({})".format(type(domain)))

        return converted_domain

    def _convert_hyperparameters(self, hyperparameters):
        """Hyperparameters conversion (from dict to TensorBoard API)"""

        from tensorboard.plugins.hparams import api as hp  # TODO move to top of file?

        assert isinstance(hyperparameters, dict), hyperparameters

        converted_hyperparameters = dict()
        for name, domain in hyperparameters.items():
            converted_domain = self._convert_domain(domain)
            if isinstance(converted_domain, tuple):
                for k, converted_sub_domain in enumerate(converted_domain):
                    assert not isinstance(converted_sub_domain, tuple)  # TODO implement?
                    sub_name = name + "_{}".format(k)
                    converted_hyperparameters[sub_name] = hp.HParam(sub_name, domain=converted_sub_domain)
            else:
                converted_hyperparameters[name] = hp.HParam(name, domain=converted_domain)

        return converted_hyperparameters

    def _sample_domain(self, domain):

        import random  # TODO move to top of file?

        if domain is None:
            sampled_value = domain
        elif isinstance(domain, (int, float, str)):
            sampled_value = domain
        elif isinstance(domain, tuple):
            sampled_value = tuple([
                self._sample_domain(sub_domain)
                for sub_domain in domain
            ])
        elif isinstance(domain, set):
            values = list(domain)
            sampled_value = random.choice(values)
        elif isinstance(domain, list):
            assert len(domain) == 2, domain
            min_value = domain[0]
            max_value = domain[1]
            if isinstance(min_value, float) and isinstance(max_value, float):
                assert min_value <= max_value, domain
                # sampled_value = random.uniform(min_value, max_value)  # i.e. uniform
                sampled_value = np.exp(random.uniform(np.log(min_value), np.log(max_value)))  # i.e. log-uniform
            elif isinstance(min_value, int) and isinstance(max_value, int):
                assert min_value <= max_value, domain
                sampled_value = random.randint(min_value, max_value)
            else:
                raise TypeError(
                    "unexpected min_value ({}) and max_value types({})".format(type(min_value), type(max_value))
                )
        else:
            # TODO correct!
            raise TypeError("unexpected domain type ({})".format(type(domain)))

        return sampled_value

    def _sample_hyperparameters(self, hyperparameters, seed=None):  # TODO move outside class?

        import random  # TODO move to top of file?

        assert isinstance(hyperparameters, dict), hyperparameters

        if seed is None:
            # random.seed(a=None)  # i.e. use current system time to initialize the random number generator.
            pass  # TODO correct?
        else:
            random.seed(a=seed)

        sampled_hyperparameters = dict()
        for name, domain in hyperparameters.items():
            sampled_value = self._sample_domain(domain)
            sampled_hyperparameters[name] = sampled_value

        return sampled_hyperparameters

    def randomized_search(self, hyperparameters, train_data, val_data, is_gpu_available=False, nb_runs=2):
        """Hyperparameter optimization/tuning."""

        from tensorboard.plugins.hparams import api as hp  # TODO move to top of file?

        if self._directory is not None:
            tensorboard_path = os.path.join(self._directory, "logs")  # TODO move to class?
            hparams_summary_path = tensorboard_path  # TODO rename?
        else:
            hparams_summary_path = None

        if not os.path.isdir(hparams_summary_path):  # i.e. search has not already been ran

            if hparams_summary_path is not None:
                # Log the experiment configuration to TensorBoard.
                converted_hyperparameters = self._convert_hyperparameters(hyperparameters)
                with tf.summary.create_file_writer(hparams_summary_path).as_default():
                    hp.hparams_config(
                        hparams=list(converted_hyperparameters.values()),
                        metrics=[
                            hp.Metric('val_loss', display_name='val_loss'),
                            hp.Metric('val_poisson', display_name='val_poisson'),
                        ],
                        # time_created_secs=None,  # i.e. current time (default)
                    )

            for run_nb in range(0, nb_runs):

                run_name = "run_{:03d}".format(run_nb)

                # Sample a random combination of hyperparameters.
                sampled_hyperparameters = self._sample_hyperparameters(hyperparameters, seed=run_nb)
                # Sanity prints.
                print("Run {:03d}/{:03d}:".format(run_nb, nb_runs))
                for name, value in sampled_hyperparameters.items():
                    print("    {}: {}".format(name, value))

                # Create model.
                filter_kwargs = copy.deepcopy(self._model.filter_kwargs)
                filter_kwargs.update({
                    'sparsity_factor': sampled_hyperparameters['sparsity_factor'],
                    'smoothness_factor': sampled_hyperparameters['smoothness_factor'],
                })
                tf.keras.backend.clear_session()  # i.e. clear TF graph
                model = self._model.__class__(
                    filter_kwargs=filter_kwargs,
                    name="ln_model",
                )
                model.compile()
                self._model = model

                # Train model.
                history = self.train(train_data, val_data, is_gpu_available=is_gpu_available, run_name=run_name)

                if hparams_summary_path is not None:
                    # run_summary_path = os.path.join(hparams_summary_path, run_name)  # TODO remove?
                    run_summary_path = os.path.join(hparams_summary_path, run_name, "train")  # TODO correct?
                    # Log the hyperparameters and metrics to TensorBoard.
                    with tf.summary.create_file_writer(run_summary_path).as_default():
                        # Log hyperparameter values for the current run/trial.
                        # TODO remove the following line (error for None value)!
                        # formatted_hyperparameters = sampled_hyperparameters
                        # TODO remove the following lines (error for multiple layers)!
                        # formatted_hyperparameters = {
                        #     name: value if value is not None else 'None'  # TODO get rid of this awful hack/patch?
                        #     for name, value in sampled_hyperparameters.items()
                        # }
                        # TODO improve/simplify the following lines!
                        formatted_hyperparameters = dict()
                        for name, value in sampled_hyperparameters.items():
                            if value is None:
                                formatted_value = 'None'
                                formatted_hyperparameters[name] = formatted_value
                            elif isinstance(value, tuple):
                                for k, sub_value in enumerate(value):
                                    sub_name = name + "_{}".format(k)
                                    if sub_value is None:
                                        formatted_value = 'None'
                                        formatted_hyperparameters[sub_name] = formatted_value
                                    else:
                                        formatted_hyperparameters[sub_name] = sub_value
                            else:
                                formatted_hyperparameters[name] = value
                        _ = hp.hparams(
                            formatted_hyperparameters,
                            # trial_id=None,  # i.e. hash of the hyperparameter values.  # TODO remove?
                            trial_id=run_name,  # TODO check!
                            # start_time_secs=None,  # i.e. current time  # TODO use option?
                        )
                        # TODO log the hyperparameters for programmatic use.
                        for name, value in formatted_hyperparameters.items():
                            name = "hyperparameters/{}".format(name)
                            value = value.item()  # i.e. numpy.float to float  # TODO keep?
                            tf.summary.scalar(name, value, step=0, description=None)  # TODO correct?
                        # Log metrics.
                        for step, val_loss in enumerate(history.history['val_loss']):
                            tf.summary.scalar('val_loss', val_loss, step=step)
                        for step, val_poisson in enumerate(history.history['val_poisson']):
                            tf.summary.scalar('val_poisson', val_poisson, step=step)
                        # TODO use callbacks instead of writing these directly?

        else:

            logger.debug("randomized search has already been ran")

        # TODO reload best model?

        return

    def load(self, run_name=None):
        """Load model."""

        # Load trained weights (if possible).
        try:
            self._model.load_weights(
                self._get_checkpoint_weights_path(tag='final', run_name=run_name)
                # by_name=False,
                # skip_mismatch=False,
            ).expect_partial()  # c.f. https://stackoverflow.com/questions/58289342/tf2-0-translation-model-error-when-restoring-the-saved-model-unresolved-objec.
            self._run_name = run_name
        except (tf.errors.NotFoundError, ValueError):
            raise FileNotFoundError

        # Test model (if possible, to make the loading effective).
        if self._test_data is not None:
            test_x, test_y = self._test_data
            _ = self._model.evaluate(test_x, test_y, batch_size=32, verbose=0)
        else:
            raise NotImplementedError("TODO")

        return

    def evaluate(self, test_data, batch_size=32):  # TODO correct `batch_size`?
        """Evaluate model."""

        metrics_names = self._model.metrics_names
        test_x, test_y = test_data
        if test_y.ndim == 3:
            test_y = np.average(test_y, axis=0)  # i.e. average over repetitions
        losses = self._model.evaluate(
            test_x,
            test_y,
            batch_size=batch_size,
        )
        logger.debug("metrics_names: {}".format(metrics_names))
        logger.debug("losses: {}".format(losses))
        ans = {
            metric_name: loss
            for metric_name, loss in zip(metrics_names, losses)
        }

        return ans

    def predict(self, test_data, batch_size=32):  # TODO correct `batch_size`?
        """Predict model output."""

        test_x, test_y = test_data
        predictions = self._model.predict(
            test_x,
            batch_size=batch_size,
        )
        if test_y.ndim == predictions.ndim + 1:
            # Add dimension for repetitions.
            reps = (test_y.shape[0],) + (test_y.ndim - 1) * (1,)
            predictions = np.tile(predictions, reps)
        assert predictions.shape == test_y.shape, (predictions.shape, test_y.shape)

        return predictions

    def predict_activation(self, test_data):
        """Predict model activation."""

        test_x, test_y = test_data
        activation = self._model.filter(test_x)
        activation = self._model.flatten(activation)
        activation = activation.numpy()
        if test_y.ndim == 3:
            nb_repetitions, _, _ = test_y.shape
            activation = np.tile(activation, (nb_repetitions, 1, 1))

        return activation

    def predict_local_spike_triggered_average(self, test_data):

        test_x, _ = test_data
        stimulus = tf.Variable(tf.cast(test_x, tf.float32))
        with tf.GradientTape() as tape:
            response = self._model(stimulus)
            gradients = tape.gradient(response, stimulus)
            # TODO normalize gradient?
        # logger.debug("gradients: {}".format(gradients))
        logger.debug("type(gradients): {}".format(type(gradients)))
        gradients = gradients.numpy()
        logger.debug("type(gradients): {}".format(type(gradients)))
        logger.debug("gradients.shape: {}".format(gradients.shape))
        # nb_batches, nb_rows, nb_columns, nb_channels = gradients.shape
        lstas = gradients[:, :, :, 0]

        return lstas

    predict_lstas = predict_local_spike_triggered_average

    def get_tensorboard_scalars(self, run_name):

        assert self._directory is not None, self._directory

        # Load event multiplexer.
        from tensorboard.backend.event_processing import event_multiplexer
        size_guidance = {
            "distributions": 500,
            "images": 4,
            "audio": 4,
            "scalars": 10000,
            "histograms": 1,
            # "tensors": 10,
            "tensors": 1000,
        }
        em = event_multiplexer.EventMultiplexer(
            # run_path_map=None,
            size_guidance=size_guidance,
            # purge_orphaned_data=True,
        )
        directory = os.path.join(self._directory, "logs")
        em.AddRunsFromDirectory(
            directory,
            # name=None
        )
        logger.debug("event_multiplexer.Runs(): {}".format(em.Runs()))

        # Get event accumulator.
        ea = em.GetAccumulator("{}/train".format(run_name))
        # ea = em.GetAccumulator("{}/validation".format(run_name))
        ea.Reload()

        # Get scalars.
        scalar_keys = ea.scalars.Keys()
        logger.debug("Scalar keys: {}".format(scalar_keys))
        scalars = dict()
        for scalar_key in scalar_keys:
            scalars[scalar_key] = dict()
            scalar_events = ea.Scalars(scalar_key)
            scalars[scalar_key]['wall_time'] = np.array([
                scalar_event.wall_time
                for scalar_event in scalar_events
            ])
            scalars[scalar_key]['step'] = np.array([
                scalar_event.step
                for scalar_event in scalar_events
            ])
            scalars[scalar_key]['value'] = np.array([
                scalar_event.value
                for scalar_event in scalar_events
            ])

        return scalars

    def get_tensorboard_tensors(self, run_name):

        assert self._directory is not None, self._directory

        # Load event multiplexer.
        from tensorboard.backend.event_processing import event_multiplexer
        size_guidance = {
            "distributions": 500,
            "images": 4,
            "audio": 4,
            "scalars": 10000,
            "histograms": 1,
            # "tensors": 10,
            "tensors": 1000,
        }
        em = event_multiplexer.EventMultiplexer(
            # run_path_map=None,
            size_guidance=size_guidance,
            # purge_orphaned_data=True,
        )
        directory = os.path.join(self._directory, "logs")
        em.AddRunsFromDirectory(
            directory,
            # name=None
        )
        logger.debug("Run keys: {}".format(em.Runs()))

        # Get event accumulator.
        ea = em.GetAccumulator("{}/train".format(run_name))
        # ea = em.GetAccumulator("{}/validation".format(run_name))
        ea.Reload()

        # Get tensors.
        tensor_keys = ea.tensors.Keys()
        logger.debug("Tensor keys: {}".format(tensor_keys))
        tensors = dict()
        for tensor_key in tensor_keys:
            tensors[tensor_key] = dict()
            tensor_events = ea.Tensors(tensor_key)
            tensors[tensor_key]['wall_time'] = np.array([
                tensor_event.wall_time
                for tensor_event in tensor_events
            ])
            tensors[tensor_key]['step'] = np.array([
                tensor_event.step
                for tensor_event in tensor_events
            ])
            tensors[tensor_key]['value'] = np.array([
                tf.make_ndarray(tensor_event.tensor_proto)
                for tensor_event in tensor_events
            ])

        return tensors

    def get_randomized_search_table(self, nb_runs, test_data=None, force=False):

        if self._directory is not None:
            path = os.path.join(self._directory, "randomized_search_table.csv")
        else:
            path = None

        if path is None or not os.path.isfile(path) or force:
            # Collect data.
            smoothness_factors = []
            sparsity_factors = []
            val_poissons = []
            val_losses = []
            for run_nb in range(0, nb_runs):
                run_name = "run_{:03d}".format(run_nb)
                tensorboard_tensors = self.get_tensorboard_tensors(run_name)
                smoothness_factors.append(tensorboard_tensors["hyperparameters/smoothness_factor"]["value"][-1])
                sparsity_factors.append(tensorboard_tensors["hyperparameters/sparsity_factor"]["value"][-1])
                val_poissons.append(tensorboard_tensors["val_poisson"]["value"][-1])
                val_losses.append(tensorboard_tensors["val_loss"]["value"][-1])
            data = {
                'smoothness_factor': np.array(smoothness_factors),
                'sparsity_factor': np.array(sparsity_factors),
                'val_poisson': np.array(val_poissons),
                'val_loss': np.array(val_losses),
            }
            # Collect additional data.
            if test_data is not None:
                test_poissons = []
                test_accuracies = []
                for run_nb in range(0, nb_runs):
                    run_name = "run_{:03d}".format(run_nb)
                    self.load(run_name=run_name)
                    _, test_y_true = test_data
                    test_y_pred = self.predict(test_data)
                    print(test_y_pred.shape)
                    if test_y_true.shape != test_y_pred.shape:  # i.e. problem due to repetitions
                        assert test_y_true.ndim == 3, test_y_true.shape
                        nb_repetitions, nb_conditions, nb_cells = test_y_true.shape
                        test_y_pred = np.tile(test_y_pred, (nb_repetitions, 1, 1))
                        assert test_y_true.shape == test_y_pred.shape, (test_y_true.shape, test_y_pred.shape)
                    # test_poisson = tf.keras.losses.poisson(test_y_true, test_y_pred)
                    test_poisson = np.mean(test_y_pred - test_y_true * np.log(test_y_pred))
                    test_poissons.append(test_poisson)
                    # test_accuracy = corrcoef(np.ravel(test_y_true), np.ravel(test_y_pred))
                    test_accuracy = corrcoef(
                        np.mean(test_y_true[0::2, :, 0], axis=0),
                        np.mean(test_y_pred[0::2, :, 0], axis=0)
                    )
                    test_accuracies.append(test_accuracy)
                additional_data = {
                    'test_poisson': np.array(test_poissons),
                    'test_accuracy': np.array(test_accuracies),
                }
                data.update(additional_data)
            # Create data frame.
            df = pd.DataFrame.from_dict(data)
            # Save to file.
            df.to_csv(
                path,
                index=True,  # i.e. write row names
            )
            print("Randomized search table saved to:")
            print("  {}".format(path))
        else:
            # Load from file.
            df = pd.read_csv(
                path,
                index_col=0,
            )

        return df

    def get_kernel(self):

        model = self._model
        # model = self._get_checkpoint(tag=tag)
        logger.debug("[weight.name for weight in model.weights]: {}".format([weight.name for weight in model.weights]))
        weights = {
            weight.name: weight
            for weight in model.weights
        }
        kernels = weights["ln_model/filter/kernel:0"]
        kernels = kernels.numpy()
        logger.debug("kernels.shape: {}".format(kernels.shape))
        # nb_rows, nb_columns, nb_channels, nb_filters = kernels.shape
        kernel = kernels[:, :, 0, 0]  # i.e. discard channel and filter dimensions

        return kernel

    def get_nonlinearity(self):

        model = self._model
        # model = self._get_checkpoint(tag=tag)
        logger.debug("[weight.name for weight in model.weights]: {}".format([weight.name for weight in model.weights]))
        weights = {
            weight.name: weight
            for weight in model.weights
        }
        alpha = weights["ln_model/parametric_softplus/alpha:0"].numpy()[0]
        beta = weights["ln_model/parametric_softplus/beta:0"].numpy()[0]
        gamma = weights["ln_model/parametric_softplus/gamma:0"].numpy()[0]

        return alpha, beta, gamma

    def get_local_spike_triggered_averages(self, test_data, append_grey_image=False):

            test_x, _ = test_data
            # nb_images, height, width, nb_channels = test_x.shape
            if append_grey_image:
                grey_value = np.mean(test_x)
                # print(grey_value)
                grey_shape = (1,) + test_x.shape[1:]
                grey_image = grey_value * np.ones(grey_shape)
                grey_image = grey_image.astype(test_x.dtype)
                test_x = np.concatenate((test_x, grey_image))
                test_data = test_x, None
            lstas = self.predict_lsta(test_data)
            # print(lstas.shape)

            return lstas

    get_local_stas = get_local_spike_triggered_averages  # i.e. alias
    get_lstas = get_local_spike_triggered_averages  # i.e. alias

    # Plotters.

    def _get_plot_path(self, with_run_name=False):

        if self._directory is not None:
            path = os.path.join(self._directory, "plots")
            if with_run_name and self._run_name is not None:
                path = os.path.join(path, self._run_name)
            if not os.path.isdir(path):
                os.makedirs(path)
        else:
            path = None

        return path

    def plot_predictions(self, test_data):
        """Plot model predictions."""

        _, test_y = test_data
        nb_repetitions, nb_conditions, nb_cells = test_y.shape
        true_y = np.mean(test_y, axis=0)  # i.e. mean over repetitions
        # nb_conditions, nb_cells = true_y.shape
        pred_y = self.predict(test_data)

        # fig, ax = plt.subplots()
        fig, ax = plt.subplots(figsize=(3.0 * 1.6, 3.0 * 1.6))
        ax.set_aspect('equal')
        #
        for repetitions_nb in range(0, nb_repetitions):
            x = test_y[repetitions_nb, :, :]
            y = pred_y[repetitions_nb, :, :]
            s = 1 ** 2
            label = "repeated" if repetitions_nb == 0 else None
            ax.scatter(x, y, s=s, color='gray', label=label)
        #
        x = true_y
        y = np.mean(pred_y, axis=0)
        s = 2 ** 2
        ax.scatter(x, y, s=s, color='black', label="averaged")
        # Add diagonal.
        # v_max = max(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(test_y), np.max(pred_y))
        v_max = max(np.max(test_y), np.max(pred_y))
        ax.plot([0.0, v_max], [0.0, v_max], color='gray', linewidth=0.3)
        # Set axis tick locations.
        ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.xaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # ax.yaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Set axis labels.
        ax.set_xlabel("data")
        ax.set_ylabel("prediction")
        # Add legend.
        ax.legend()
        fig.tight_layout()

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "predictions.pdf")
            fig.savefig(output_path)
            print("Figure saved to:\n  {}".format(output_path))
            plt.close(fig)

        return

    def plot_randomized_search_summary(self, nb_runs, val_data=None, test_data=None):

        # Collect data.
        df = self.get_randomized_search_table(nb_runs)
        smoothness_factors = df['smoothness_factor'].to_numpy()
        sparsity_factors = df['sparsity_factor'].to_numpy()
        val_poissons = df['val_poisson'].to_numpy()
        test_accuracies = df['test_accuracy'].to_numpy()
        if val_data is not None:
            _, val_y_true = val_data
            nb_conditions, nb_cells = val_y_true.shape
            # Compute validation poisson for the "mean" model.
            val_y_pred = np.mean(val_y_true, axis=(0,))
            val_y_pred = np.tile(val_y_pred, (nb_conditions, 1))
            val_y_pred = np.maximum(val_y_pred, 1e-8)
            assert val_y_pred.shape == val_y_true.shape, (val_y_pred.shape, val_y_true.shape)
            mean_val_poisson = np.mean(val_y_pred - val_y_true * np.log(val_y_pred))
            # psth_val_poisson = None  # can not be computed (no repetitions)
        else:
            mean_val_poisson = None
        if test_data is not None:
            _, test_y_true = test_data
            nb_repetitions, nb_conditions, nb_cells = test_y_true.shape
            # Compute test poisson for the "mean" model.
            test_y_pred = np.mean(test_y_true, axis=(0, 1))
            test_y_pred = np.tile(test_y_pred, (nb_repetitions, nb_conditions, 1))
            test_y_pred = np.maximum(test_y_pred, 1e-8)
            assert test_y_pred.shape == test_y_true.shape, (test_y_pred.shape, test_y_true.shape)
            mean_test_poisson = np.mean(test_y_pred - test_y_true * np.log(test_y_pred))
            # Compute test poisson for the "PSTH" model.
            test_y_pred = np.mean(test_y_true, axis=(0,))
            test_y_pred = np.tile(test_y_pred, (nb_repetitions, 1, 1))
            test_y_pred = np.maximum(test_y_pred, 1e-8)
            assert test_y_pred.shape == test_y_true.shape, (test_y_pred.shape, test_y_true.shape)
            psth_test_poisson = np.mean(test_y_pred - test_y_true * np.log(test_y_pred))
        else:
            mean_test_poisson = None
            psth_test_poisson = None

        nb_rows = 2
        nb_columns = 2
        fig, axes = plt.subplots(
            nrows=nb_rows, ncols=nb_columns, squeeze=False,
            figsize=(float(nb_columns) * 3.0 * 1.6, float(nb_rows) * 3.0 * 1.6)
        )

        # Plot smoothness factors vs sparsity factors.
        ax = axes[1, 0]
        # Set axis scales.
        ax.set_xscale('log')
        ax.set_yscale('log')
        x = sparsity_factors
        y = smoothness_factors
        s = 3 ** 2
        c = val_poissons
        val_poisson_ref = np.quantile(val_poissons, 0.05)
        edgecolors = [
            'tab:red' if val_poisson < val_poisson_ref else 'none'
            for val_poisson in val_poissons
        ]
        pc = ax.scatter(x, y, s=s, c=c, edgecolors=edgecolors)
        # Set axis tick locations.
        # ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Set axis labels.
        ax.set_xlabel("sparsity factor")
        ax.set_ylabel("smoothness factor")
        # Tight layout.
        fig.tight_layout()
        # Add colorbar.
        from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        axins = inset_axes(ax, width="3%", height="50%", loc='upper right')
        cb = fig.colorbar(pc, cax=axins, orientation="vertical")
        cb.set_label("validation loss")
        axins.yaxis.set_ticks_position('left')
        axins.yaxis.set_label_position('left')
        axins.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))

        # Plot validation losses vs sparsity factors.
        ax = axes[0, 0]
        # Set axis scales.
        ax.set_xscale('log')
        ax.set_yscale('linear')
        x = sparsity_factors
        y = val_poissons
        s = 3 ** 2
        c = val_poissons
        val_poisson_ref = np.quantile(val_poissons, 0.05)
        edgecolors = [
            'tab:red' if val_poisson < val_poisson_ref else 'none'
            for val_poisson in val_poissons
        ]
        pc = ax.scatter(x, y, s=s, c=c, edgecolors=edgecolors)
        linewidth = 0.3
        linestyle = (0, (5 / linewidth, 5 / linewidth))  # i.e. dashed (adapted)
        ax.axhline(y=mean_val_poisson, color='tab:grey', linewidth=linewidth, linestyle='solid', zorder=0)
        ax.axhline(y=mean_test_poisson, color='tab:grey', linewidth=linewidth, linestyle=linestyle, zorder=0)
        ax.axhline(y=psth_test_poisson, color='tab:grey', linewidth=linewidth, linestyle=linestyle, zorder=0)
        # Set axis tick locations.
        # ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Set axis labels.
        ax.set_xlabel("sparsity factor")
        ax.set_ylabel("validation loss")
        # # Tight layout.
        # fig.tight_layout()
        # # Add colorbar.
        # from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        # axins = inset_axes(ax, width="3%", height="50%", loc='upper right')
        # cb = fig.colorbar(pc, cax=axins, orientation="vertical")
        # cb.set_label("validation loss")
        # axins.yaxis.set_ticks_position('left')
        # axins.yaxis.set_label_position('left')
        # axins.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        _ = pc

        # Plot validation losses vs smoothness factors.
        ax = axes[0, 1]
        # Set axis scales.
        ax.set_xscale('log')
        ax.set_yscale('linear')
        x = smoothness_factors
        y = val_poissons
        s = 3 ** 2
        c = val_poissons
        val_poisson_ref = np.quantile(val_poissons, 0.05)
        edgecolors = [
            'tab:red' if val_poisson < val_poisson_ref else 'none'
            for val_poisson in val_poissons
        ]
        pc = ax.scatter(x, y, s=s, c=c, edgecolors=edgecolors)
        linewidth = 0.3
        linestyle = (0, (5 / linewidth, 5 / linewidth))  # i.e. dashed (adapted)
        ax.axhline(y=mean_val_poisson, color='tab:grey', linewidth=linewidth, linestyle='solid', zorder=0)
        ax.axhline(y=mean_test_poisson, color='tab:grey', linewidth=linewidth, linestyle=linestyle, zorder=0)
        ax.axhline(y=psth_test_poisson, color='tab:grey', linewidth=0.3, linestyle=linestyle, zorder=0)
        # Set axis tick locations.
        # ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Set axis labels.
        ax.set_xlabel("smoothness factor")
        ax.set_ylabel("validation loss")
        # # Tight layout.
        # fig.tight_layout()
        # # Add colorbar.
        # from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        # axins = inset_axes(ax, width="3%", height="50%", loc='upper right')
        # cb = fig.colorbar(pc, cax=axins, orientation="vertical")
        # cb.set_label("validation loss")
        # axins.yaxis.set_ticks_position('left')
        # axins.yaxis.set_label_position('left')
        # axins.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        _ = pc

        # Plot test accuracies vs validation losses.
        ax = axes[1, 1]
        # Set axis scales.
        ax.set_xscale('linear')
        ax.set_yscale('linear')
        x = val_poissons
        y = test_accuracies
        s = 3 ** 2
        c = val_poissons
        val_poisson_ref = np.quantile(val_poissons, 0.05)
        edgecolors = [
            'tab:red' if val_poisson < val_poisson_ref else 'none'
            for val_poisson in val_poissons
        ]
        pc = ax.scatter(x, y, s=s, c=c, edgecolors=edgecolors)
        # Add reference lines.
        linewidth = 0.3
        linestyle = (0, (5 / linewidth, 5 / linewidth))  # i.e. dashed (adapted)
        ax.axvline(x=mean_val_poisson, color='tab:grey', linewidth=linewidth, linestyle='solid', zorder=0)
        ax.axvline(x=mean_test_poisson, color='tab:grey', linewidth=linewidth, linestyle=linestyle, zorder=0)
        ax.axvline(x=psth_test_poisson, color='tab:grey', linewidth=0.3, linestyle=linestyle, zorder=0)
        ax.axhline(y=0.0, color='tab:grey', linewidth=linewidth, linestyle='solid', zorder=0)
        ax.axhline(y=1.0, color='tab:grey', linewidth=linewidth, linestyle='solid', zorder=0)
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Set axis labels.
        ax.set_xlabel("validation loss")
        ax.set_ylabel("test accuracy")
        # # Tight layout.
        # fig.tight_layout()
        _ = pc

        # Add annotation.
        ax = axes[0, 0]
        if self._name is not None:
            text = self._name
            ax.annotate(
                text, (0.0, 1.0), (+7.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )

        # Save plot (if necessary).
        plot_path = self._get_plot_path()
        if plot_path is not None:
            output_path = os.path.join(plot_path, "randomized_search.pdf")
            fig.savefig(output_path)
            print("Figure saved to:\n  {}".format(output_path))
            plt.close(fig)

        return

    def plot_activation_map(self, image):

        # stimulus = tf.Variable(tf.cast(image, tf.float32))
        # logger.debug("stimulus: {}".format(stimulus))
        # response = self._model(stimulus)
        # logger.debug("response: {}".format(response))

        # logger.debug("self._model.summary(): {}".format(self._model.summary()))
        #
        # self._model.run_eagerly = True  # TODO remove?
        # self._model.predict(
        #     image,
        #     batch_size=32,
        # )
        #
        # logger.debug("self._model.summary(): {}".format(self._model.summary()))
        #
        # # TODO AttributeError: The layer has never been called and thus has no defined input shape.
        #
        # filter_layer = self._model.get_layer('filter')
        # logger.debug("dir(filter_layer): {}".format(dir(filter_layer)))
        # logger.debug("filter_layer.input_shape: {}".format(filter_layer.input_shape))
        # logger.debug("filter_layer.output_shape: {}".format(filter_layer.output_shape))
        # logger.debug("filter_layer.outbound_nodes: {}".format(filter_layer.outbound_nodes))
        # logger.debug("filter_layer.inbound_nodes: {}".format(filter_layer.inbound_nodes))
        # logger.debug("filter_layer.output: {}".format(filter_layer.output))
        # logger.debug("filter_layer.input: {}".format(filter_layer.input))
        #
        # activation_model = tf.keras.models.Model(
        #     self._model.inputs,
        #     # outputs=[self._model.get_layer('filter').output],
        #     outputs=[filter_layer.output],
        #     # outputs=[self._model.filter.output],
        #     # outputs=[self._model.layers['filter'].output],
        # )

        activation = self._model.filter(image)
        print(activation)  # TODO remove!
        print(type(activation))
        activation = activation.numpy()
        print(activation)
        print(type(activation))
        activation = activation[0, 0, 0, 0]
        print(activation)
        print(type(activation))

        raise NotImplementedError("TODO")

    def plot_gradient(self, image):

        stimulus = tf.Variable(tf.cast(image, tf.float32))
        with tf.GradientTape() as tape:
            response = self._model(stimulus)
            gradient = tape.gradient(response, stimulus)
            # TODO normalize gradient?
        # logger.debug("gradient: {}".format(gradient))
        logger.debug("type(gradient): {}".format(type(gradient)))
        gradient = gradient.numpy()
        logger.debug("type(gradient): {}".format(type(gradient)))
        logger.debug("gradient.shape: {}".format(gradient.shape))
        # nb_batches, nb_rows, nb_columns, nb_channels = gradient.shape
        gradient = gradient[0, :, :, 0]

        image = image[0, :, :, 0]
        fig, axes = plt.subplots(nrows=1, ncols=2, squeeze=False)
        # Plot image.
        ax = axes[0, 0]
        vmin = - np.max(np.abs(image))
        vmax = + np.max(np.abs(image))
        ax.imshow(image, cmap='RdBu_r', vmin=vmin, vmax=vmax)
        # Plot gradient.
        ax = axes[0, 1]
        vmin = - np.max(np.abs(gradient))
        vmax = + np.max(np.abs(gradient))
        ax.imshow(gradient, cmap='RdBu_r', vmin=vmin, vmax=vmax)

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "gradient.pdf")
            fig.savefig(output_path)
            print("Figure saved to:\n  {}".format(output_path))
            plt.close(fig)

        return

    def plot_kernel(self):

        kernel = self.get_kernel()

        # fig, ax = plt.subplots()
        fig, ax = plt.subplots(figsize=(3.0 * 1.6, 3.0 * 1.6))
        vmin = - np.max(np.abs(kernel))
        vmax = + np.max(np.abs(kernel))
        ax.imshow(kernel, cmap='RdBu_r', vmin=vmin, vmax=vmax)

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "kernel.pdf")
            fig.savefig(output_path)
            plt.close(fig)

        return

    def plot_model_summary(self, test_data=None):

        # Collect data.
        #
        assert self._run_name is not None
        tensorboard_scalars = self.get_tensorboard_scalars(self._run_name)
        tensorboard_tensors = self.get_tensorboard_tensors(self._run_name)
        #
        kernel = self.get_kernel()
        #
        nonlinearity = self.get_nonlinearity()
        #
        if test_data is not None:
            test_x, test_y = test_data
            test_activation = self.predict_activation(test_data)
            test_y_pred = self.predict(test_data)
        else:
            test_x, test_y = None, None
            test_activation = None
            test_y_pred = None

        # Create figure.
        nb_rows = 1
        nb_columns = 5
        figsize = (
            float(nb_columns) * 2.0 * 1.6,
            float(nb_rows) * 2.0 * 1.6,
        )
        fig, axes = plt.subplots(nrows=nb_rows, ncols=nb_columns, squeeze=False, figsize=figsize)

        # Plot loss & metric through epoch.
        ax = axes[0, 0]
        # # Set axis scales.
        # ax.set_xscale('linear')
        # ax.set_yscale('log')
        # Plot train loss.
        x = tensorboard_scalars["epoch_loss"]["step"]
        y = tensorboard_scalars["epoch_loss"]["value"]
        c = 'C0'
        alpha = 0.4
        ax.plot(x, y, c=c, alpha=alpha, label="train loss")
        # # Plot train poisson.
        x = tensorboard_scalars["epoch_poisson"]["step"]
        y = tensorboard_scalars["epoch_poisson"]["value"]
        c = 'C0'
        ax.plot(x, y, c=c, label="train Poisson")
        # # Plot validation loss.
        x = tensorboard_tensors["val_loss"]["step"]
        y = tensorboard_tensors["val_loss"]["value"]
        c = 'C1'
        alpha = 0.4
        ax.plot(x, y, c=c, alpha=alpha, label="val. loss")
        # # Plot validation Poisson.
        x = tensorboard_tensors["val_poisson"]["step"]
        y = tensorboard_tensors["val_poisson"]["value"]
        c = 'C1'
        ax.plot(x, y, c=c, label="val. Poisson")
        # # Set axis limits.
        # ax.set_xlim(x[0], x[-1])
        # # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # # Set axis locators.
        ax.xaxis.set_major_locator(plt.FixedLocator([x[0], x[-1]]))
        ax.yaxis.set_major_locator(plt.MaxNLocator(2))
        # # Set axis labels.
        ax.set_xlabel("epoch")
        # ax.set_ylabel("value")
        # # Set title.
        ax.set_title(r"losses \& metrics")
        # # Add annotation.
        final_val_poisson = tensorboard_tensors["val_poisson"]["value"][-1]
        ax.annotate(
            "final val. Poisson\n" + r"$\simeq " + "{:.3f}".format(final_val_poisson) + "$",
            # xy=(0.0, 0.0),
            # xytext=(0.0, -50.0),
            xy=(0.75, 0.25),
            xytext=(0.0, 0.0),
            xycoords='axes fraction',
            textcoords='offset points',
            # horizontalalignment='left',
            horizontalalignment='center',
        )
        # # Add legend.
        ax.legend()

        # Plot filter.
        ax = axes[0, 1]
        # # Plot kernel.
        vmin = - np.max(np.abs(kernel))
        vmax = + np.max(np.abs(kernel))
        ai = ax.imshow(kernel, cmap='RdBu_r', vmin=vmin, vmax=vmax)
        # # Set axis locators.
        ax.xaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[1] - 1]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[0] - 1]))
        # # # Set axis labels.
        # ax.set_xlabel("x")
        # ax.set_ylabel("y")
        # # Set title.
        # ax.set_title("kernel")
        # ax.set_title("filter")
        ax.set_title("linear filter")
        # # # Add colorbar.
        # from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        # axins = inset_axes(ax, width="3%", height="50%", loc='upper right')
        # cb = fig.colorbar(ai, cax=axins, orientation="vertical")
        # # cb.set_label("weight")
        # axins.yaxis.set_ticks_position('left')
        # axins.yaxis.set_label_position('left')
        # axins.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        _ = ai
        # # Add annotations.
        sparsity_factor = tensorboard_tensors["hyperparameters/sparsity_factor"]["value"][0]
        smoothness_factor = tensorboard_tensors["hyperparameters/smoothness_factor"]["value"][0]
        ax.annotate(
            # r"$\lambda_{\text{L1}} \simeq " + "{:.1e}".format(sparsity_factor).replace("e", r"\times 10^{") + "}$, "
            # + r"$\lambda_{\Delta} \simeq " + "{:.1e}".format(smoothness_factor).replace("e", r"\times 10^{") + "}$",
            r"$\lambda_{\text{L1}} \simeq " + "{:.2g}".format(sparsity_factor) + "$, "
            + r"$\lambda_{\Delta} \simeq " + "{:.2g}".format(smoothness_factor) + "$",
            # xy=(0.0, 0.0),
            # xytext=(0.0, -50.0),
            xy=(0.5, 0.0),
            xytext=(0.0, +5.0),
            xycoords='axes fraction',
            textcoords='offset points',
            # horizontalalignment='left',
            horizontalalignment='center',
        )

        # Plot non-linearity.
        ax = axes[0, 2]
        # Plot data (if possible).
        if test_data is not None:
            x = np.ravel(test_activation)
            y = np.ravel(test_y)
            s = 1 ** 2
            c = 'gray'
            ax.scatter(x, y, s=s, c=c, label="raster")
            x = np.ravel(np.mean(test_activation, axis=0))
            y = np.ravel(np.mean(test_y, axis=0))
            s = 2 ** 2
            c = 'black'
            ax.scatter(x, y, s=s, c=c, label="PSTH")
        # # Plot fitted activation.
        if test_data is None:
            x_min = -10.0
            x_max = +10.0
        else:
            x_min = - 1.05 * np.max(np.abs(test_activation))
            x_max = + 1.05 * np.max(np.abs(test_activation))
        x = np.linspace(x_min, x_max, num=100, dtype=np.float32)
        y = self._model.activation(x)
        ax.plot(x, y, c='tab:red', label="model")
        # y = np.log(1.0 + np.exp(x))
        # ax.plot(x, y, c='grey', linewidth=0.3)
        # # Plot test output vs test activation (if possible).
        # # Add horizontal and vertical lines.
        # ax.axhline(y=0.0, color='gray', linewidth=0.3)
        # ax.axvline(x=0.0, color='gray', linewidth=0.3)
        # # Set axis limits.
        # ax.set_ylim(top=y_max)
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # Set axis locators.
        # ax.xaxis.set_major_locator(plt.FixedLocator([x[0], 0, x[-1]]))
        ax.xaxis.set_major_locator(plt.MaxNLocator(3))
        ax.yaxis.set_major_locator(plt.MaxNLocator(2))
        # Set axis labels.
        ax.set_xlabel("generator signal")
        ax.set_ylabel("spike count")
        # # Set title.
        # ax.set_title("non-linearity")
        ax.set_title("nonlinear transform")
        # # Add legend.
        ax.legend()
        # # Add annotations.
        alpha, beta, gamma = nonlinearity
        ax.annotate(
            r"$\alpha \simeq " + "{:.2f}$\n".format(alpha)
            + r"$\beta \simeq " + "{:.2f}$\n".format(beta)
            + r"$\gamma \simeq " + "{:.2f}$".format(gamma),
            xy=(0.0, 0.5),
            xytext=(+7.0, 0.0),
            xycoords='axes fraction',
            textcoords='offset points',
            horizontalalignment='left',
            verticalalignment='center',
        )

        # Plot performance.
        ax = axes[0, 3]
        ax.set_aspect('equal')
        # # Plot prediction vs data.
        x = np.ravel(test_y[0::2, :, :])  # i.e. even repetitions only
        # y = np.ravel(np.tile(test_y_pred, (test_y.shape[0] // 2, 1, 1)))  # TODO remove (deprecated)?
        y = np.ravel(test_y_pred[0::2, :, :])  # i.e. even repetitions only
        s = 1 ** 2
        # c = (0.5 * np.array([[31, 119, 180]]) + 0.5 * np.array([[255, 255, 255]])) / 255
        # # i.e. 0.5 'C0' + 0.5 'white'
        c = (0.5 * np.array([[214, 39, 40]]) + 0.5 * np.array([[255, 255, 255]])) / 255
        # i.e. 0.5 'C2' + 0.5 'white'
        ax.scatter(x, y, s=s, c=c, label="raster")
        # # Plot prediction vs PSTH.
        x = np.mean(test_y[0::2, :, :], axis=0)
        # y = test_y_pred  # TODO remove (deprecated)?
        y = np.mean(test_y_pred[0::2, :, :], axis=0)
        s = 2 ** 2
        # c = np.array([[31, 119, 180]]) / 255  # i.e. 'C0'
        c = np.array([[214, 39, 40]]) / 255  # i.e. 'C2'
        ax.scatter(x, y, s=s, c=c, label="PSTH")
        # # Add diagonal.
        # v_max = max(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(test_y), np.max(pred_y))
        v_max = max(np.max(test_y), np.max(test_y_pred))
        ax.plot([0.0, v_max], [0.0, v_max], color='gray', linewidth=0.3)
        # # Set axis tick locations.
        ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.xaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # ax.yaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # # Set axis labels.
        ax.set_xlabel("measured spike count\n(even repetitions)")
        ax.set_ylabel("predicted spike count")
        # # Set title.
        # ax.set_title("non-linearity")
        ax.set_title("performance")
        # # Add legend.
        ax.legend()
        # # Add annotations.
        accuracy = corrcoef(
            np.mean(test_y[0::2, :, 0], axis=0),
            # test_y_pred[:, 0],  # TODO remove (deprecated)?
            np.mean(test_y_pred[0::2, :, 0], axis=0)
        )
        ax.annotate(
            r"$r \simeq " + "{:.3g}".format(accuracy) + "$",
            xy=(1.0, 0.5),
            xytext=(0.0, -7.0),
            xycoords='axes fraction',
            textcoords='offset points',
            horizontalalignment='right',
            verticalalignment='center',
        )

        # Plot data quality.
        ax = axes[0, 4]
        ax.set_aspect('equal')
        # # Plot prediction vs data.
        x = np.ravel(test_y[0::2, :, :])
        y = np.ravel(test_y[1::2, :, :])
        s = 1 ** 2
        c = 'gray'
        ax.scatter(x, y, s=s, c=c, label="rasters")
        # # Plot prediction vs PSTH.
        x = np.mean(test_y[0::2, :, :], axis=0)
        y = np.mean(test_y[1::2, :, :], axis=0)
        s = 2 ** 2
        c = 'black'
        ax.scatter(x, y, s=s, c=c, label="PSTHs")
        # # Add diagonal.
        # v_max = max(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(test_y), np.max(pred_y))
        v_max = max(np.max(test_y), np.max(test_y_pred))
        ax.plot([0.0, v_max], [0.0, v_max], color='gray', linewidth=0.3)
        # # Set axis tick locations.
        ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.xaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # ax.yaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # # Set axis labels.
        ax.set_xlabel("measured spike count\n(even repetitions)")
        ax.set_ylabel("measured spike count\n(odd repetitions)")
        # # Set title.
        # ax.set_title("non-linearity")
        ax.set_title("data quality")
        # # Add legend.
        ax.legend()
        # # Add annotations.
        reliability = corrcoef(
            np.mean(test_y[0::2, :, 0], axis=0),
            np.mean(test_y[1::2, :, 0], axis=0),
        )
        ax.annotate(
            r"$r \simeq " + "{:.3g}".format(reliability) + "$",
            xy=(1.0, 0.5),
            xytext=(0.0, -7.0),
            xycoords='axes fraction',
            textcoords='offset points',
            horizontalalignment='right',
        )

        # Tight layout.
        #fig.tight_layout()

        # Add annotations.
        ax = axes[0, 0]
        if self._name is not None:
            text = self._name
            ax.annotate(
                text, (0.0, 1.0), (+7.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )
        if self._run_name is not None:
            text = self._run_name.replace("run_", "r")
            ax.annotate(
                text, (0.0, 1.0), (+7.0 + 25.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )

        # TODO add colorbars?

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "model_summary.pdf")
            #fig.savefig(output_path)
            print("Figure saved to:\n {}".format(output_path))
            plt.close(fig)

        return

    def plot_local_stas(
            self, test_data, nb_columns=10, sta_ellipse_parameters_path=None, cell_nb=None, pixel_size=None
    ):

        test_x, _ = test_data
        images = test_x[:, :, :, 0]
        nb_images, _, _ = images.shape
        lstas = self.predict_lstas(test_data)

        # Load STA ellipse (if possible).
        if sta_ellipse_parameters_path is not None and cell_nb is not None and pixel_size is not None:
            # Load ellipse.
            df = pd.read_csv(sta_ellipse_parameters_path, index_col=0)
            parameters = df.loc[cell_nb]
            xy = (parameters['x'], parameters['y'])
            w = parameters['w']
            h = parameters['h']
            a = parameters['a']
            ellipse = pcs.Ellipse(xy, w, h, angle=a, fill=False, color='tab:green', alpha=0.5)
            # Set image extent.
            _, width, height = images.shape
            left = -0.5 * float(width) * pixel_size
            right = +0.5 * float(width) * pixel_size
            bottom = -0.5 * float(height) * pixel_size
            top = +0.5 * float(height) * pixel_size
            extent = (left, right, bottom, top)
            # Set axis limits.
            zoom_window = 1.0e-3
            zoom_window = min(zoom_window, right - left, top - bottom)
            x_0, y_0 = xy
            x_0 = max(x_0, left + 0.5 * zoom_window)
            x_0 = min(x_0, right - 0.5 * zoom_window)
            y_0 = max(y_0, bottom + 0.5 * zoom_window)
            y_0 = min(y_0, top - 0.5 * zoom_window)
            x_limits = (x_0 - 0.5 * zoom_window, x_0 + 0.5 * zoom_window)
            y_limits = (y_0 - 0.5 * zoom_window, y_0 + 0.5 * zoom_window)
        else:
            ellipse = None
            extent = None
            x_limits = None
            y_limits = None

        # Create figure.
        nb_rows = 2 * ((nb_images - 1) // nb_columns + 1)
        # figsize = (
        #     float(nb_columns) * 1.0 * 1.6,
        #     float(nb_rows) * 1.0 * 1.6,
        # )
        # figsize = (6.4, 4.8)  # i.e. Matplotlib's default
        # figsize = (5.0, 7.5)  # i.e. LaTeX's default (in in)
        # figsize = (5.0, 3.0)  # good
        # figsize = (5.0, 2.5)  # bad
        figsize = (
            5.0,
            float(nb_rows) * 5.0 / float(nb_columns)
        )
        gridspec_kwargs = {
            'left': 0.02,  # (fraction of figure width)
            'right': 0.98,
            'top': 0.98,  # (fraction of figure height)
            'bottom': 0.02,
            'wspace': 0.05,  # (fraction of average axis width)
            'hspace': 0.05,  # (fraction of average axis height)
        }
        fig, axes = plt.subplots(
            nrows=nb_rows, ncols=nb_columns, squeeze=False, gridspec_kw=gridspec_kwargs, figsize=figsize
        )

        # Hide axis for each subplot.
        for ax in axes.flat:
            ax.set_axis_off()

        # image_imshow_kwargs = {
        #     'cmap': 'RdBu_r',
        #     'vmin': - 0.25 * np.max(np.abs(images)),
        #     'vmax': + 0.25 * np.max(np.abs(images)),
        #     'extent': extent,
        # }
        image_imshow_kwargs = {
            'cmap': 'Greys_r',
            'vmin': np.min(images),
            'vmax': np.max(images),
            'extent': extent,
        }

        lsta_imshow_kwargs = {
            'cmap': 'RdBu_r',
            'vmin': - np.max(np.abs(lstas)),
            'vmax': + np.max(np.abs(lstas)),
            'extent': extent,
        }

        for k in range(0, nb_images):

            # Plot image.
            image = images[k]
            row_nb = 2 * (k // nb_columns)
            column_nb = k % nb_columns
            ax = axes[row_nb, column_nb]
            ax.set_axis_on()
            ax.imshow(image, **image_imshow_kwargs)
            ax.set_xticks([])
            ax.set_yticks([])
            if ellipse is not None:
                ellipse = pcs.Ellipse(xy, w, h, angle=a, fill=False, color='tab:green', alpha=0.5)
                ax.add_patch(ellipse)
                ax.set_xlim(*x_limits)  # i.e. zoom
                ax.set_ylim(*y_limits)  # i.e. zoom
                plot_scale_bar(ax, scale=250.0e-6, unit='m', loc='lower left', with_label=False)

            # Plot LSTA.
            lsta = lstas[k]
            row_nb = 2 * (k // nb_columns) + 1
            column_nb = k % nb_columns
            ax = axes[row_nb, column_nb]
            ax.set_axis_on()
            ax.imshow(lsta, **lsta_imshow_kwargs)
            ax.set_xticks([])
            ax.set_yticks([])
            if ellipse is not None:
                ellipse = pcs.Ellipse(xy, w, h, angle=a, fill=False, color='tab:green', alpha=0.5)
                ax.add_patch(ellipse)
                ax.set_xlim(*x_limits)  # i.e. zoom
                ax.set_ylim(*y_limits)  # i.e. zoom
                plot_scale_bar(ax, scale=250e-6, unit='m', loc='lower left', with_label=(k == 0))

        # # Tight layout.
        # fig.tight_layout()

        # Add annotations.
        ax = axes[0, 0]
        if self._name is not None:
            text = self._name
            ax.annotate(
                text, (0.0, 1.0), (+7.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )
        if self._run_name is not None:
            text = self._run_name.replace("run_", "r")
            ax.annotate(
                text, (0.0, 1.0), (+7.0 + 25.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "local_stas.pdf")
            fig.savefig(output_path)
            print("Figure saved to:\n  {}".format(output_path))
            plt.close(fig)

        return


    def save_local_spike_triggered_averages(self, data, filename="lstas.npy", append_grey_image=False, lazy=False):

        assert self._directory is not None
        assert self._run_name is not None
        path = os.path.join(self._directory, self._run_name, filename)

        if not lazy or not os.path.isfile(path):

            x, _ = data
            if append_grey_image:
                grey_value = np.mean(x)
                print(grey_value)  # TODO comment!
                grey_shape = (1,) + x.shape[1:]
                grey_image = grey_value * np.ones(grey_shape)
                grey_image = grey_image.astype(x.dtype)
                x = np.concatenate((x, grey_image))
                data = x, None
            print("x.shape: {}".format(x.shape))  # TODO remove!
            nb_images, height, width, nb_channels = x.shape
            batch_size = 64
            if nb_images <= batch_size:
                lstas = self.predict_lstas(data)
            else:
                from tqdm import tqdm
                print("batch size: {} images".format(batch_size))
                nb_batches = (nb_images - 1) // batch_size + 1
                lstas = [
                    self.predict_lstas((x[batch_size*batch_nb:batch_size*(batch_nb+1)], None))
                    for batch_nb in tqdm(range(0, nb_batches), unit="batch")
                ]
                # nb_cells, batch_size, height, width = lstas[0].shape
                lstas = np.concatenate(lstas, axis=1)
            print(lstas.shape)  # TODO comment!
            # nb_cells, nb_images, height, width = lstas.shape

            # Save to disk.
            np.save(path, lstas)

        else:

            import warnings
            warnings.warn("Found {}, won't recompute the LSTAs (lazy mode activated)...".format(path))

            # Load from disk.
            lstas = np.load(path)

        return lstas

    save_local_stas = save_local_spike_triggered_averages  # i.e alias
    save_lstas = save_local_spike_triggered_averages  # i.e. alias

    def save_predictions(self, data, observations_filename="observations.npy", predictions_filename="predictions.npy"):

        x, y = data
        nb_repetitions, nb_conditions, nb_cells = y.shape
        # print(y.shape)
        y_ = np.mean(y, axis=0)  # i.e. mean over repetitions
        # nb_conditions, nb_cells = y_.shape
        y_pred_ = self.predict((x, y_))
        # nb_conditions, nb_cells = y_pred.shape
        y_pred = np.tile(y_pred_, (nb_repetitions, 1, 1))  # i.e. reconstruction dimension of repetitions
        # nb_repetitions, nb_conditions, nb_cells = y_pred.shape
        # print(y_pred.shape)

        path = os.path.join(self._directory, self._run_name, observations_filename)
        np.save(path, y)
        print("Observations saved to {}".format(path))

        path = os.path.join(self._directory, self._run_name, predictions_filename)
        np.save(path, y_pred)
        print("Predictions saved to {}".format(path))

        return