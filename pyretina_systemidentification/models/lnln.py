import copy
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf

from pyretina_systemidentification.layers.dense_nd import DenseND
from pyretina_systemidentification.initializers.gaussian_2d_kernel import Gaussian2DKernelInitializer
from pyretina_systemidentification.regularizers.l1_smooth2d import L1Smooth2DRegularizer
from pyretina_systemidentification.activations.parametric_softplus import ParametricSoftplusActivation
from pyretina_systemidentification.activations.parametric_relu import ParametricReLU
from pyretina_systemidentification.models.base import BaseModelHandler
from pyretina_systemidentification.utils import corrcoef


logger = logging.getLogger(__name__)


# Model.

class LNLNModel(tf.keras.Model):
    """LN-LN model."""

    default_model_kwargs = {
        "first_filter_size": (2 * 3 + 1, 2 * 3 + 1),  # ideal when sigma equal 30 µm
        "first_activation": "parametric_softplus",
        "smoothness_factor": 0.5e-1,  # i.e. mean optimal value for the LN model
        "sparsity_factor": 3.5e-3,  # i.e. mean optimal value for the LN model
    }

    def __init__(self, model_kwargs=None, learning_rate=1e-3, name="lnln_model", **kwargs):
        """Initialization of the model."""

        super().__init__(name=name, **kwargs)

        # Model keywords arguments.
        self.model_kwargs = copy.deepcopy(self.default_model_kwargs)
        if model_kwargs is not None:
            self.model_kwargs.update(model_kwargs)

        self.learning_rate = learning_rate

        self.losses_map = dict()

        # Add filter of first layer (i.e. convolutional layer).
        first_filters = 2  # i.e. OFF & ON filters
        first_kernel_size = self.model_kwargs["first_filter_size"]
        kernel_initializer = Gaussian2DKernelInitializer(
            # scale=24.0,  # i.e. pixel size, 8 x 3 µm
            # sigma=30.0,
        )
        first_filter_kwargs = {
            'trainable': False,  # i.e. layer's variable fixed
            'name': "first_filter",
        }
        self.first_filter = tf.keras.layers.Convolution2D(
            first_filters,
            first_kernel_size,
            # strides=(1, 1),
            # padding='valid',
            # data_format=None,
            # dilation_rate=(1, 1),
            # activation=None,
            use_bias=False,  # c.f. first activation which takes care of it
            kernel_initializer=kernel_initializer,
            # bias_initializer='zeros',
            # kernel_regularizer=None,
            # bias_regularizer=None,
            # activity_regularizer=None,
            # kernel_constraint=None,
            # c.f. beta (activation weight)
            # bias_constraint=None,
            **first_filter_kwargs,
        )

        # Add activation of first layer (ReQu).
        first_activation = self.model_kwargs["first_activation"]
        if first_activation is None:
            # Try without.
            self.first_activation = None
        elif first_activation == "requ":
            # Try ReQU.
            from pyretina_systemidentification.activations.requ import ReQU
            self.first_activation = ReQU()
        elif first_activation == "softplus":
            # Try softplus.
            first_activation_kwargs = {
                'trainable': False,  # i.e. non parametric
                'name': 'first_activation',
            }
            self.first_activation = ParametricSoftplusActivation(
                # threshold=20.0,
                # minimum=None,
                **first_activation_kwargs
            )
        elif first_activation == "parametric_softplus":
            # Try parametric softplus.
            first_activation_kwargs = {
                # 'trainable': True,
                'name': 'first_activation',
            }
            self.first_activation = ParametricSoftplusActivation(
                # threshold=20.0,
                # minimum=None,
                # weights_sharing_axes=(0, 1, 2),  # weight.shape = (rows, cols, channels)
                weights_sharing_axes=(0, 1),  # weight.shape = (rows, cols, channels)  # TODO best option?
                # alpha_initializer=1.0,
                # beta_initializer=1.0,
                # gamma_initializer=0.0,
                alpha_initializer=0.1,  # i.e. ReLU like
                beta_initializer=10.0,  # i.e. ReLU like
                gamma_initializer=0.0,
                **first_activation_kwargs
            )
        elif first_activation == "relu":
            # Try ReLU.
            self.first_activation = tf.keras.layers.ReLU(
                # max_value=None,
                # negative_slope=0,
                # threshold=0,
                # **kwargs
            )
        elif first_activation == "parametric_relu":
            # Try parametric ReLU.
            first_activation_kwargs = {
                # 'trainable': True,
                'name': 'first_activation',
            }
            self.first_activation = ParametricReLU(
                # weights_sharing_axes=(0, 1, 2),  # weight.shape = (rows, cols, filters)
                weights_sharing_axes=(0, 1),  # weight.shape = (rows, cols, filters)  # TODO best option?
                # threshold_trainable=True,  # i.e. bias
                # negative_slope_trainable=False,
                positive_slope_trainable=False,  # i.e. restrict the parametrization to the threshold
                **first_activation_kwargs
            )
        else:
            raise ValueError("unexpected value: {}".format(first_activation))

        # Add filter of second layer (i.e. dense layer).
        units = 1  # i.e. single neuron model
        kernel_regularizer = L1Smooth2DRegularizer(
            sparsity_factor=self.model_kwargs["sparsity_factor"],
            smoothness_factor=self.model_kwargs["smoothness_factor"],
        )
        kernel_initializer = tf.keras.initializers.TruncatedNormal(
            # mean=0.0,
            # stddev=0.05,  # TODO correct?
            # seed=None,
        )
        # TODO try another kernel initializer (e.g. inverse filtered STA)?
        kernel_constraint = tf.keras.constraints.MaxNorm(
            max_value=1.0,
            axis=(0, 1),  # kernel.shape = (rows, cols, filters)
        )
        second_filter_kwargs = {
            'trainable': True,
            'name': "second_filter",
        }
        self.second_filter = DenseND(
            units,
            # activation=None,
            use_bias=False,  # TODO correct?
            kernel_initializer=kernel_initializer,
            # bias_initializer='zeros',
            kernel_regularizer=kernel_regularizer,
            # bias_regularizer=None,
            # activity_regularizer=None,
            kernel_constraint=kernel_constraint,
            # bias_constraint=None,
            **second_filter_kwargs,
        )

        # Add activation of second layer (parametric softplus).
        second_activation_kwargs = {
            # 'trainable': True,
            'name': 'second_activation',
        }
        self.second_activation = ParametricSoftplusActivation(
            # threshold=20.0,
            minimum=1.0e-8,  # i.e. help learning with Poisson loss  # TODO correct value?
            weights_sharing_axes=(0,),  # weight.shape = (units,)
            # alpha_trainable=True,
            # beta_trainable=True,
            # gamma_trainable=True,
            **second_activation_kwargs
        )

    def compile(self, **kwargs):
        """Configure the learning process of the model."""

        if self._is_compiled:
            logger.warning("Model has already been compiles.")
        else:
            # optimizer_kwargs = dict()
            optimizer = tf.keras.optimizers.Adam(
                learning_rate=self.learning_rate,
                # beta_1=0.9,
                # beta_2=0.999,
                # epsilon=1e-07,
                # amsgrad=False,
                name='Adam',
                # **optimizer_kwargs
            )
            loss = tf.keras.losses.Poisson(
                # reduction=losses_utils.ReductionV2.AUTO,
                name='poisson'
            )
            metrics = [
                tf.keras.metrics.Poisson(
                    name='poisson',
                    # dtype=None
                )
            ]
            super().compile(
                optimizer=optimizer,
                loss=loss,
                metrics=metrics,
                # loss_weights=None,
                # sample_weight_mode=None,
                # weighted_metrics=None,
                # target_tensors=None,
                # distribute=None,
                # **kwargs
            )

        return

    def call(self, inputs, **kwargs):
        """Forward computation of the model."""

        internals = inputs

        # Apply first filter.
        internals = self.first_filter(internals)
        # Apply first activation (if necessary).
        if self.first_activation is not None:
            internals = self.first_activation(internals)
        # Apply second filter.
        internals = self.second_filter(internals)
        # # # Register loss associated to the second filter.
        assert len(self.second_filter.losses) == 1, self.second_filter.losses
        loss_name = self.second_filter.kernel.name
        loss_name = loss_name[loss_name.find('/') + 1:]  # i.e. remove model name
        loss_name = loss_name[:loss_name.find(':')]  # i.e. remove trailing index
        loss_name = "regularization/{}".format(loss_name)  # i.e. add prefix
        loss_value = self.second_filter.losses[0]
        self.losses_map[loss_name] = loss_value
        # Apply second activation.
        internals = self.second_activation(internals)

        outputs = internals

        # Add one metric for each loss.
        logger.debug("self.losses: {}".format(self.losses))
        logger.debug("self.losses_map: {}".format(self.losses_map))
        for loss_name, loss_value in self.losses_map.items():
            self.add_metric(loss_value, aggregation='mean', name=loss_name)

        return outputs

    def create_handler(self, *args, **kwargs):

        return _LNLNModelHandler(self, *args, **kwargs)


create = LNLNModel  # i.e. alias


# Model handler.

class _LNLNModelHandler(BaseModelHandler):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

    def get_first_kernels(self):

        model = self._model
        # model = self._get_checkpoint(tag=tag)
        weights = {
            weight.name: weight
            for weight in model.weights
        }
        # kernels = weights["ln_model/first_filter/kernel:0"]
        kernels = weights["lnln_model/first_filter/kernel:0"]
        kernels = kernels.numpy()
        logger.debug("kernels.shape: {}".format(kernels.shape))
        # nb_rows, nb_columns, nb_channels, nb_filters = kernels.shape
        kernels = kernels[:, :, 0, :]
        kernels = np.moveaxis(kernels, -1, 0)

        return kernels

    def plot_gradient(self, image):

        stimulus = tf.Variable(tf.cast(image, tf.float32))
        with tf.GradientTape() as tape:
            response = self._model(stimulus)
            gradient = tape.gradient(response, stimulus)
            # TODO normalize gradient?
        # logger.debug("gradient: {}".format(gradient))
        logger.debug("type(gradient): {}".format(type(gradient)))
        gradient = gradient.numpy()
        logger.debug("type(gradient): {}".format(type(gradient)))
        logger.debug("gradient.shape: {}".format(gradient.shape))
        # nb_batches, nb_rows, nb_columns, nb_channels = gradient.shape
        gradient = gradient[0, :, :, 0]

        image = image[0, :, :, 0]
        fig, axes = plt.subplots(nrows=1, ncols=2, squeeze=False)
        # Plot image.
        ax = axes[0, 0]
        vmin = - np.max(np.abs(image))
        vmax = + np.max(np.abs(image))
        ax.imshow(image, cmap='RdBu_r', vmin=vmin, vmax=vmax)
        # Plot gradient.
        ax = axes[0, 1]
        vmin = - np.max(np.abs(gradient))
        vmax = + np.max(np.abs(gradient))
        ax.imshow(gradient, cmap='RdBu_r', vmin=vmin, vmax=vmax)

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "gradient.pdf")
            fig.savefig(output_path)
            print("Figure saved to:\n  {}".format(output_path))
            plt.close(fig)

        return


    def get_second_kernels(self):

        model = self._model
        # model = self._get_checkpoint(tag=tag)
        weights = {
            weight.name: weight
            for weight in model.weights
        }
        # kernels = weights["ln_model/second_filter/kernel:0"]
        kernels = weights["lnln_model/second_filter/kernel:0"]
        kernels = kernels.numpy()
        # kernels = kernels[:, 0]
        # shape = (108 - (7 - 1), 108 - (7 - 1), 2)  # TODO generalize!
        # kernels = np.reshape(kernels, shape)
        # kernels = np.moveaxis(kernels, -1, 0)
        logger.debug("kernels.shape: {}".format(kernels.shape))
        # nb_rows, nb_columns, nb_channels, nb_units = kernels.shape
        kernels = kernels[:, :, :, 0]
        kernels = np.moveaxis(kernels, -1, 0)

        return kernels

    def plot_model_summary(self, test_data=None):

        # Collect data.
        #
        assert self._run_name is not None
        tensorboard_scalars = self.get_tensorboard_scalars(self._run_name)
        tensorboard_tensors = self.get_tensorboard_tensors(self._run_name)
        # # Get kernels of the first layer.
        first_kernels = self.get_first_kernels()
        # # Get kernels of the second layer.
        second_kernels = self.get_second_kernels()
        # TODO remove the following commented lines?
        # #
        # nonlinearity = self.get_nonlinearity()
        #
        if test_data is not None:
            test_x, test_y = test_data
            # test_activation = self.predict_activation(test_data)  # TODO remove?
            test_y_pred = self.predict(test_data)
        else:
            test_x, test_y = None, None
            # test_activation = None  # TODO remove?
            test_y_pred = None

        # Create figure.
        nb_rows = 1
        nb_columns = 5
        figsize = (
            float(nb_columns) * 2.0 * 1.6,
            float(nb_rows) * 2.0 * 1.6,
        )
        fig, axes = plt.subplots(nrows=nb_rows, ncols=nb_columns, squeeze=False, figsize=figsize)

        # Plot loss & metric through epoch.
        ax = axes[0, 0]
        # # Set axis scales.
        # ax.set_xscale('linear')
        # ax.set_yscale('log')
        # Plot train loss.
        x = tensorboard_scalars["epoch_loss"]["step"]
        y = tensorboard_scalars["epoch_loss"]["value"]
        c = 'C0'
        alpha = 0.4
        ax.plot(x, y, c=c, alpha=alpha, label="train loss")
        # # Plot train poisson.
        x = tensorboard_scalars["epoch_poisson"]["step"]
        y = tensorboard_scalars["epoch_poisson"]["value"]
        c = 'C0'
        ax.plot(x, y, c=c, label="train Poisson")
        # # Plot validation loss.
        x = tensorboard_tensors["val_loss"]["step"]
        y = tensorboard_tensors["val_loss"]["value"]
        c = 'C1'
        alpha = 0.4
        ax.plot(x, y, c=c, alpha=alpha, label="val. loss")
        # # Plot validation Poisson.
        x = tensorboard_tensors["val_poisson"]["step"]
        y = tensorboard_tensors["val_poisson"]["value"]
        c = 'C1'
        ax.plot(x, y, c=c, label="val. Poisson")
        # # Set axis limits.
        # ax.set_xlim(x[0], x[-1])
        # # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # # Set axis locators.
        ax.xaxis.set_major_locator(plt.FixedLocator([x[0], x[-1]]))
        ax.yaxis.set_major_locator(plt.MaxNLocator(2))
        # # Set axis labels.
        ax.set_xlabel("epoch")
        # ax.set_ylabel("value")
        # # Set title.
        ax.set_title(r"losses \& metrics")
        # # Add annotation.
        final_val_poisson = tensorboard_tensors["val_poisson"]["value"][-1]
        ax.annotate(
            "final val. Poisson\n" + r"$\simeq " + "{:.3f}".format(final_val_poisson) + "$",
            # xy=(0.0, 0.0),
            # xytext=(0.0, -50.0),
            xy=(0.75, 0.25),
            xytext=(0.0, 0.0),
            xycoords='axes fraction',
            textcoords='offset points',
            # horizontalalignment='left',
            horizontalalignment='center',
        )
        # # Add legend.
        ax.legend()

        # Plot OFF kernel of the second layer.
        ax = axes[0, 1]
        # # Plot kernel.
        vmin = - np.max(np.abs(second_kernels))
        vmax = + np.max(np.abs(second_kernels))
        kernel = second_kernels[0]
        ai = ax.imshow(kernel, cmap='RdBu_r', vmin=vmin, vmax=vmax)
        # # Set axis locators.
        ax.xaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[1] - 1]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[0] - 1]))
        # # # Set axis labels.
        # ax.set_xlabel("x")
        # ax.set_ylabel("y")
        # # Set title.
        ax.set_title("OFF filter")
        # # # Add colorbar.
        # from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        # axins = inset_axes(ax, width="3%", height="50%", loc='upper right')
        # cb = fig.colorbar(ai, cax=axins, orientation="vertical")
        # # cb.set_label("weight")
        # axins.yaxis.set_ticks_position('left')
        # axins.yaxis.set_label_position('left')
        # axins.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        _ = ai
        # # Add annotations.
        sparsity_factor = tensorboard_tensors["hyperparameters/sparsity_factor"]["value"][0]
        smoothness_factor = tensorboard_tensors["hyperparameters/smoothness_factor"]["value"][0]
        ax.annotate(
            # r"$\lambda_{\text{L1}} \simeq " + "{:.1e}".format(sparsity_factor).replace("e", r"\times 10^{") + "}$, "
            # + r"$\lambda_{\Delta} \simeq " + "{:.1e}".format(smoothness_factor).replace("e", r"\times 10^{") + "}$",
            r"$\lambda_{\text{L1}} \simeq " + "{:.2g}".format(sparsity_factor) + "$, "
            + r"$\lambda_{\Delta} \simeq " + "{:.2g}".format(smoothness_factor) + "$",
            # xy=(0.0, 0.0),
            # xytext=(0.0, -50.0),
            xy=(0.5, 0.0),
            xytext=(0.0, +5.0),
            xycoords='axes fraction',
            textcoords='offset points',
            # horizontalalignment='left',
            horizontalalignment='center',
        )

        # Plot ON kernel of the second layer.
        ax = axes[0, 2]
        # # Plot kernel.
        vmin = - np.max(np.abs(second_kernels))
        vmax = + np.max(np.abs(second_kernels))
        kernel = second_kernels[1]
        ai = ax.imshow(kernel, cmap='RdBu_r', vmin=vmin, vmax=vmax)
        # # Set axis locators.
        ax.xaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[1] - 1]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0, kernel.shape[0] - 1]))
        # # # Set axis labels.
        # ax.set_xlabel("x")
        # ax.set_ylabel("y")
        # # Set title.
        ax.set_title("ON filter")
        # # # Add colorbar.
        # from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        # axins = inset_axes(ax, width="3%", height="50%", loc='upper right')
        # cb = fig.colorbar(ai, cax=axins, orientation="vertical")
        # # cb.set_label("weight")
        # axins.yaxis.set_ticks_position('left')
        # axins.yaxis.set_label_position('left')
        # axins.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        _ = ai
        # # Add annotations.
        sparsity_factor = tensorboard_tensors["hyperparameters/sparsity_factor"]["value"][0]
        smoothness_factor = tensorboard_tensors["hyperparameters/smoothness_factor"]["value"][0]
        ax.annotate(
            # r"$\lambda_{\text{L1}} \simeq " + "{:.1e}".format(sparsity_factor).replace("e", r"\times 10^{") + "}$, "
            # + r"$\lambda_{\Delta} \simeq " + "{:.1e}".format(smoothness_factor).replace("e", r"\times 10^{") + "}$",
            r"$\lambda_{\text{L1}} \simeq " + "{:.2g}".format(sparsity_factor) + "$, "
            + r"$\lambda_{\Delta} \simeq " + "{:.2g}".format(smoothness_factor) + "$",
            # xy=(0.0, 0.0),
            # xytext=(0.0, -50.0),
            xy=(0.5, 0.0),
            xytext=(0.0, +5.0),
            xycoords='axes fraction',
            textcoords='offset points',
            # horizontalalignment='left',
            horizontalalignment='center',
        )

        # Plot performance.
        ax = axes[0, 3]  # TODO correct?
        ax.set_aspect('equal')
        # # Plot prediction vs data.
        x = np.ravel(test_y[0::2, :, :])
        # y = np.ravel(np.tile(test_y_pred, (test_y.shape[0] // 2, 1, 1)))  # deprecated
        y = np.ravel(test_y_pred[0::2, :, :])
        s = 1 ** 2
        # c = (0.5 * np.array([[31, 119, 180]]) + 0.5 * np.array([[255, 255, 255]])) / 255
        # # i.e. 0.5 'C0' + 0.5 'white'
        c = (0.5 * np.array([[214, 39, 40]]) + 0.5 * np.array([[255, 255, 255]])) / 255
        # i.e. 0.5 'C2' + 0.5 'white'
        ax.scatter(x, y, s=s, c=c, label="raster")
        # # Plot prediction vs PSTH.
        x = np.mean(test_y[0::2, :, :], axis=0)
        # y = test_y_pred  # deprecated
        y = np.mean(test_y_pred[0::2, :, :], axis=0)
        s = 2 ** 2
        # c = np.array([[31, 119, 180]]) / 255  # i.e. 'C0'
        c = np.array([[214, 39, 40]]) / 255  # i.e. 'C2'
        ax.scatter(x, y, s=s, c=c, label="PSTH")
        # # Add diagonal.
        # v_max = max(np.max(true_y), np.max(pred_y))  # deprecated
        # v_max = min(np.max(true_y), np.max(pred_y))  #d eprecated
        # v_max = min(np.max(test_y), np.max(pred_y))  # deprecated
        # v_max = max(np.max(test_y), np.max(test_y_pred))  # deprecated
        v_max = max(np.max(test_y), np.max(test_y_pred))
        ax.plot([0.0, v_max], [0.0, v_max], color='gray', linewidth=0.3)
        # # Set axis tick locations.
        ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.xaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # ax.yaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # # Set axis labels.
        ax.set_xlabel("measured spike count\n(even repetitions)")
        ax.set_ylabel("predicted spike count")
        # # Set title.
        # ax.set_title("non-linearity")
        ax.set_title("performance")
        # # Add legend.
        ax.legend()
        # # Add annotations.
        accuracy = corrcoef(
            np.mean(test_y[0::2, :, 0], axis=0),
            # test_y_pred[:, 0],  # deprecated
            np.mean(test_y_pred[0::2, :, 0], axis=0),
        )
        ax.annotate(
            r"$r \simeq " + "{:.3g}".format(accuracy) + "$",
            xy=(1.0, 0.5),
            xytext=(0.0, -7.0),
            xycoords='axes fraction',
            textcoords='offset points',
            horizontalalignment='right',
            verticalalignment='center',
        )

        # Plot data quality.
        ax = axes[0, 4]  # TODO correct?
        ax.set_aspect('equal')
        # # Plot prediction vs data.
        x = np.ravel(test_y[0::2, :, :])
        y = np.ravel(test_y[1::2, :, :])
        s = 1 ** 2
        c = 'gray'
        ax.scatter(x, y, s=s, c=c, label="rasters")
        # # Plot prediction vs PSTH.
        x = np.mean(test_y[0::2, :, :], axis=0)
        y = np.mean(test_y[1::2, :, :], axis=0)
        s = 2 ** 2
        c = 'black'
        ax.scatter(x, y, s=s, c=c, label="PSTHs")
        # # Add diagonal.
        # v_max = max(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(true_y), np.max(pred_y))
        # v_max = min(np.max(test_y), np.max(pred_y))
        v_max = max(np.max(test_y), np.max(test_y_pred))
        ax.plot([0.0, v_max], [0.0, v_max], color='gray', linewidth=0.3)
        # # Set axis tick locations.
        ax.xaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        ax.yaxis.set_major_locator(plt.MaxNLocator(nbins=2))
        # ax.xaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # ax.yaxis.set_major_locator(plt.LinearLocator(numticks=2))
        # # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        # # Set axis labels.
        ax.set_xlabel("measured spike count\n(even repetitions)")
        ax.set_ylabel("measured spike count\n(odd repetitions)")
        # # Set title.
        # ax.set_title("non-linearity")
        ax.set_title("data quality")
        # # Add legend.
        ax.legend()
        # # Add annotations.
        reliability = corrcoef(
            np.mean(test_y[0::2, :, 0], axis=0),
            np.mean(test_y[1::2, :, 0], axis=0),
        )
        ax.annotate(
            r"$r \simeq " + "{:.3g}".format(reliability) + "$",
            xy=(1.0, 0.5),
            xytext=(0.0, -7.0),
            xycoords='axes fraction',
            textcoords='offset points',
            horizontalalignment='right',
        )

        # Tight layout.
        #fig.tight_layout()

        # Add annotations.
        ax = axes[0, 0]
        if self._name is not None:
            text = self._name
            ax.annotate(
                text, (0.0, 1.0), (+7.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )
        if self._run_name is not None:
            text = self._run_name.replace("run_", "r")
            ax.annotate(
                text, (0.0, 1.0), (+7.0 + 25.0, -7.0),
                xycoords='figure fraction', textcoords='offset points',
                horizontalalignment='left', verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='tab:grey')
            )

        # TODO add colorbars?

        # Save plot (if necessary).
        plot_path = self._get_plot_path(with_run_name=True)
        if plot_path is not None:
            output_path = os.path.join(plot_path, "model_summary.pdf")
            #fig.savefig(output_path)
            plt.close(fig)

        return

    def predict_local_spike_triggered_average(self, test_data):

        test_x, _ = test_data
        stimulus = tf.Variable(tf.cast(test_x, tf.float32))
        with tf.GradientTape() as tape:
            response = self._model(stimulus)
            gradients = tape.gradient(response, stimulus)
            # TODO normalize gradient?
        # logger.debug("gradients: {}".format(gradients))
        logger.debug("type(gradients): {}".format(type(gradients)))
        gradients = gradients.numpy()
        logger.debug("type(gradients): {}".format(type(gradients)))
        logger.debug("gradients.shape: {}".format(gradients.shape))
        # nb_batches, nb_rows, nb_columns, nb_channels = gradients.shape
        lstas = gradients[:, :, :, 0]

        return lstas

    predict_lstas = predict_local_spike_triggered_average  # i.e. alias

    def save_local_spike_triggered_averages(self, data, filename="lstas.npy", append_grey_image=False, lazy=False):

        assert self._directory is not None
        assert self._run_name is not None
        path = os.path.join(self._directory, self._run_name, filename)

        if not lazy or not os.path.isfile(path):

            x, _ = data
            if append_grey_image:
                grey_value = np.mean(x)
                print(grey_value)  # TODO comment!
                grey_shape = (1,) + x.shape[1:]
                grey_image = grey_value * np.ones(grey_shape)
                grey_image = grey_image.astype(x.dtype)
                x = np.concatenate((x, grey_image))
                data = x, None
            print("x.shape: {}".format(x.shape))  # TODO remove!
            nb_images, height, width, nb_channels = x.shape
            batch_size = 64
            if nb_images <= batch_size:
                lstas = self.predict_lstas(data)
            else:
                from tqdm import tqdm
                print("batch size: {} images".format(batch_size))
                nb_batches = (nb_images - 1) // batch_size + 1
                lstas = [
                    self.predict_lstas((x[batch_size*batch_nb:batch_size*(batch_nb+1)], None))
                    for batch_nb in tqdm(range(0, nb_batches), unit="batch")
                ]
                # nb_cells, batch_size, height, width = lstas[0].shape
                lstas = np.concatenate(lstas, axis=1)
            print(lstas.shape)  # TODO comment!
            # nb_cells, nb_images, height, width = lstas.shape

            # Save to disk.
            np.save(path, lstas)

        else:

            import warnings
            warnings.warn("Found {}, won't recompute the LSTAs (lazy mode activated)...".format(path))

            # Load from disk.
            lstas = np.load(path)

        return lstas

    save_local_stas = save_local_spike_triggered_averages  # i.e alias
    save_lstas = save_local_spike_triggered_averages  # i.e. alias