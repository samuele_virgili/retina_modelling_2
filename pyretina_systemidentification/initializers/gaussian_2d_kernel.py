import logging
import numpy as np
import tensorflow as tf

from tensorflow.python.framework import dtypes


logger = logging.getLogger(__name__)


class Gaussian2DKernelInitializer(tf.keras.initializers.Initializer):
    """Initializer that generates kernels with Gaussian 2D curves."""

    def __init__(self, scale=24.0, sigma=30.0):

        super().__init__()

        self.scale = scale  # i.e. pixel size, 8 x 3 µm
        self.sigma = sigma

    def __call__(self, shape, dtype=dtypes.float32):
        """Returns a tensor object initialized as specified by the initializer.
        Arguments:
            shape: Shape of the tensor.
            dtype: Optional dtype of the tensor. Only floating point types are
                supported.
        Raises:
            ValueError: If the dtype is not floating point
        """

        logger.debug("shape: {}".format(shape))
        logger.debug("dtype: {}".format(dtype))

        assert len(shape) == 4, shape
        nb_rows, nb_columns, nb_channels, nb_filters = shape
        assert nb_channels == 1, nb_channels
        assert nb_filters == 2, nb_filters

        off_sigma = self.sigma
        on_sigma = self.sigma

        # Get the coordinates of the grid of pixels.
        x = self.scale * np.arange(0, nb_rows, dtype=dtype.as_numpy_dtype())
        x = x - np.mean(x)
        y = self.scale * np.arange(0, nb_columns, dtype=dtype.as_numpy_dtype())
        y = y - np.mean(y)
        xv, yv = np.meshgrid(x, y)

        # Create OFF kernel.
        off_kernel = \
            - (1.0 / (2.0 * np.pi * off_sigma ** 2)) \
            * np.exp(- (np.power(xv, 2) + np.power(yv, 2)) / (2.0 * off_sigma ** 2))
        off_kernel = off_kernel * self.scale ** 2  # i.e. normalization
        off_kernel = off_kernel[:, :, np.newaxis]

        logger.debug("off_kernel.shape: {}".format(off_kernel.shape))

        # Create ON kernel.
        on_kernel = \
            + (1.0 / (2.0 * np.pi * on_sigma ** 2)) \
            * np.exp(- (np.power(xv, 2) + np.power(yv, 2)) / (2.0 * on_sigma ** 2))
        on_kernel = on_kernel * self.scale ** 2  # i.e. normalization
        on_kernel = on_kernel[:, :, np.newaxis]

        logger.debug("on_kernel.shape: {}".format(on_kernel.shape))

        # Stack kernels.
        gaussian_kernel = np.stack([
            off_kernel,
            on_kernel,
        ], axis=-1)

        # logger.debug("gaussian_kernel: {}".format(gaussian_kernel))
        logger.debug("gaussian_kernel.shape: {}".format(gaussian_kernel.shape))
        np.save("/tmp/gaussian_kernel.npy", gaussian_kernel)

        return gaussian_kernel

    def get_config(self):

        config = super().get_config()
        config.update({
            'scale': self.scale,
            'sigma': self.sigma,
        })

        return config
