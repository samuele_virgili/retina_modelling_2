import logging
import tensorflow as tf


from tensorflow.python.eager import context
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import tensor_shape
from tensorflow.python.keras import activations
from tensorflow.python.keras import backend
from tensorflow.python.keras import constraints
from tensorflow.python.keras import initializers
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.engine.input_spec import InputSpec
# from tensorflow.python.ops import gen_math_ops
# from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn
# from tensorflow.python.ops import sparse_ops
from tensorflow.python.ops import standard_ops


logger = logging.getLogger(__name__)


class DenseND(tf.keras.layers.Layer):
    """Densely-connected layer (with a non-flat weight tensor).

    This layer implements the operation:
    `output = activation(dot(input, kernel) + bias)`
    where `activation` is the element-wise activation function passed as the `activation` argument,
    where `kernel` is a weights tensor created by the layer,
    and where `bias` is a bias vector created by the layer (only applicable if `use_bias` is `True`).
    Note: If the input to the layer has a rank greater than 2, then the weight tensor shape is adapted correspondingly.

    Arguments:
        units: integer
            Dimensionality of the output space. Must be positive.
        activation: none or ... (optional)
            Activation function to use.
            If you don't specify anything, no activation is applied (i.e. "linear" activation: `a(x) = x`).
        use_bias: boolean (optional)
            Whether the layer uses a bias vector.
        kernel_initializer: none or ... (optional)
            Initializer for the `kernel` weights tensor.
        bias_initializer: none or ... (optional)
            Initializer for the bias vector.
        kernel_regularizer: none or ... (optional)
            Regularizer function applied to the `kernel` weights tensor.
        bias_regularizer: none or ... (optional)
            Regularizer function applied to the bias vector.
        activity_regularizer: none or ... (optional)
            Regularizer function applied to the output of the layer (its "activation")..
        kernel_constraint: none or ... (optional)
            Constraint function applied to the `kernel` weights tensor.
        bias_constraint: none or ... (optional)
            Constraint function applied to the bias vector.

    Input shape:
        N-D tensor with shape: `(batch_size, input_dim_1, ..., input_dim_n)`.
        The most common situation would be a 2D input with shape `(batch_size, input_dim)`.

    Output shape:
        N-D tensor with shape: `(batch_size, units)`.
        For the most common situation, the output would have shape `(batch_size, units)`.
    """

    def __init__(
        self,
        units,
        activation=None,
        use_bias=True,
        kernel_initializer='glorot_uniform',
        bias_initializer='zeros',
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        kernel_constraint=None,
        bias_constraint=None,
        **kwargs
    ):

        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)

        super().__init__(
            activity_regularizer=regularizers.get(activity_regularizer),
            **kwargs
        )

        units = int(units) if not isinstance(units, int) else units
        assert units > 0, units

        self.units = units
        self.activation = activations.get(activation)
        self.use_bias = use_bias
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.bias_initializer = initializers.get(bias_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.bias_regularizer = regularizers.get(bias_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)
        self.bias_constraint = constraints.get(bias_constraint)

        self.supports_masking = True
        self.input_spec = InputSpec(min_ndim=2)

        self.kernel = None
        self.bias = None

    def build(self, input_shape):

        dtype = dtypes.as_dtype(self.dtype or backend.floatx())
        if not (dtype.is_floating or dtype.is_complex):
            raise TypeError('Unable to build `Dense` layer with non-floating point dtype %s' % (dtype,))

        input_shape = tensor_shape.TensorShape(input_shape)
        for k in range(1, input_shape.rank):
            if tensor_shape.dimension_value(input_shape[k]) is None:
                raise ValueError(
                    "The {}th dimension of the inputs to `DenseND` should be defined. Found `None`.".format(k)
                )
        axes = {
            k: tensor_shape.dimension_value(input_shape[k])
            for k in range(1, input_shape.rank)
        }
        self.input_spec = InputSpec(
            min_ndim=2,
            axes=axes
        )
        shape = [
            axes[k]
            for k in range(1, input_shape.rank)
        ] + [
            self.units
        ]
        self.kernel = self.add_weight(
            'kernel',
            shape=shape,
            initializer=self.kernel_initializer,
            regularizer=self.kernel_regularizer,
            constraint=self.kernel_constraint,
            dtype=self.dtype,
            trainable=True
        )
        if self.use_bias:
            self.bias = self.add_weight(
                'bias',
                shape=[self.units],
                initializer=self.bias_initializer,
                regularizer=self.bias_regularizer,
                constraint=self.bias_constraint,
                dtype=self.dtype,
                trainable=True
            )
        else:
            self.bias = None
        self.built = True

    def call(self, inputs, **kwargs):

        rank = len(inputs.shape)
        logger.debug("rank: {}".format(rank))
        if rank > 2:
            # Broadcasting is required for the inputs.
            axes = [
                [k for k in range(1, rank)],  # i.e. inputs_axes = [1, ..., rank - 1]
                [k for k in range(0, rank - 1)],  # i.e. kernel_axes = [0, ..., rank - 2]
            ]
            outputs = standard_ops.tensordot(
                inputs,
                self.kernel,
                axes,
            )
            logger.debug("outputs.shape: {}".format(outputs.shape))  # i.e. (None, units)
            # Reshape the output back to the original ndim of the input.
            if not context.executing_eagerly():
                shape = inputs.shape.as_list()
                logger.debug("shape: {}".format(shape))  # i.e. [None, input_dim_1, ..., input_dim_n]
                output_shape = shape[:1] + [self.units]
                logger.debug("output_shape: {}".format(output_shape))  # i.e. [None, units]
                outputs.set_shape(output_shape)
        else:
            # inputs = math_ops.cast(inputs, self._compute_dtype)
            # if K.is_sparse(inputs):
            #     outputs = sparse_ops.sparse_tensor_dense_matmul(inputs, self.kernel)
            # else:
            #     outputs = gen_math_ops.mat_mul(inputs, self.kernel)
            raise NotImplementedError("TODO")  # TODO complete?
        if self.use_bias:
            outputs = nn.bias_add(outputs, self.bias)
        if self.activation is not None:
            return self.activation(outputs)

        return outputs

    def compute_output_shape(self, input_shape):

        input_shape = tensor_shape.TensorShape(input_shape)
        input_shape = input_shape.with_rank_at_least(2)
        for k in range(1, input_shape.rank):
            if tensor_shape.dimension_value(input_shape[k]) is None:
                raise ValueError(
                    "The {}th dimension of input_shape must be defined, but saw: {}".format(k, input_shape)
                )

        return input_shape[:1].concatenate(self.units)

    def get_config(self):

        config = super().get_config()
        config.update({
            'units': self.units,
            'activation': activations.serialize(self.activation),
            'use_bias': self.use_bias,
            'kernel_initializer': initializers.serialize(self.kernel_initializer),
            'bias_initializer': initializers.serialize(self.bias_initializer),
            'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
            'bias_regularizer': regularizers.serialize(self.bias_regularizer),
            'activity_regularizer': regularizers.serialize(self.activity_regularizer),
            'kernel_constraint': constraints.serialize(self.kernel_constraint),
            'bias_constraint': constraints.serialize(self.bias_constraint),
        })

        return config
