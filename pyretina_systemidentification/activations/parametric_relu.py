import logging

import tensorflow as tf


logger = logging.getLogger(__name__)


class ParametricReLU(tf.keras.layers.Layer):
    """Parametric Rectified Linear Unit.

    `f(x) = negative_slope * x` for `x < threshold`,
    `f(x) = positive_slope * x` for `x >= threshold`.

    Arguments:
        weights_sharing_axes: none, 'all' or tuple (optional)
            # TODO add description.
            The default value is 'all'.
        threshold_trainable: boolean (optional)
            # TODO add description.
            The default value is True.
        negative_slope_trainable: boolean (optional)
            # TODO add description.
            The default value is False.
        positive_slope_trainable: boolean (optional)
            # TODO add description.
            The default value is True.
        name: string (optional)
            # TODO add description.
            The default value is 'parametric_relu'.
        **kwargs
    """

    def __init__(
            self,
            weights_sharing_axes='all',
            threshold_trainable=True,
            negative_slope_trainable=False,
            positive_slope_trainable=True,
            name='parametric_relu',
            **kwargs
    ):
        """Input-independent initialization of the layer."""

        super().__init__(name=name, **kwargs)

        self.weights_sharing_axes = weights_sharing_axes
        self.threshold_trainable = threshold_trainable
        self.negative_slope_trainable = negative_slope_trainable
        self.positive_slope_trainable = positive_slope_trainable

        # Declare weight variables.
        self.threshold = None
        self.negative_slope = None
        self.positive_slope = None

    def build(self, input_shape):
        """Input-dependent initialization of the layer."""

        logger.debug("input_shape: {}".format(input_shape))

        # batch_size = input_shape[0]
        sample_shape = input_shape[1:]

        # Resolve weight shape.
        if self.weights_sharing_axes is None:
            shape = sample_shape
        elif self.weights_sharing_axes == 'all':
            shape = len(sample_shape) * (1,)
        elif isinstance(self.weights_sharing_axes, (tuple, list)):
            shape = list(sample_shape)
            for axis in self.weights_sharing_axes:
                shape[axis] = 1
            shape = tuple(shape)
        else:
            raise ValueError("unexpected value: {}".format(self.weights_sharing_axes))

        logger.debug("shape: {}".format(shape))

        # Add threshold weight.
        self.threshold = self.add_weight(
            name='threshold',
            shape=shape,
            dtype=tf.float32,
            initializer=tf.keras.initializers.Constant(value=0.0),
            # regularizer=None,
            trainable=self.threshold_trainable,
            # constraint=None,
            # partitioner=None,
            # use_resource=None,
            # synchronization=tf.VariableSynchronization.AUTO,
            # aggregation=tf.compat.v1.VariableAggregation.NONE,
            # **kwargs
        )

        # Add negative slope weight.
        self.negative_slope = self.add_weight(
            name='negative_slope',
            shape=shape,
            dtype=tf.float32,
            initializer=tf.keras.initializers.Constant(value=0.0),
            # regularizer=None,
            trainable=self.negative_slope_trainable,
            constraint=tf.keras.constraints.NonNeg(),  # TODO relax?
            # partitioner=None,
            # use_resource=None,
            # synchronization=tf.VariableSynchronization.AUTO,
            # aggregation=tf.compat.v1.VariableAggregation.NONE,
            # **kwargs
        )

        # Add positive slope weight.
        self.positive_slope = self.add_weight(
            name='positive_slope',
            shape=shape,
            dtype=tf.float32,
            initializer=tf.keras.initializers.Constant(value=1.0),
            # regularizer=None,
            trainable=self.positive_slope_trainable,
            constraint=tf.keras.constraints.NonNeg(),
            # partitioner=None,
            # use_resource=None,
            # synchronization=tf.VariableSynchronization.AUTO,
            # aggregation=tf.compat.v1.VariableAggregation.NONE,
            # **kwargs
        )

        logger.debug("self.threshold.shape: {}".format(self.threshold.shape))
        logger.debug("self.negative_slope.shape: {}".format(self.negative_slope.shape))
        logger.debug("self.positive_slope.shape: {}".format(self.positive_slope.shape))

        return

    def call(self, inputs, **kwargs):
        """Forward computation of the layer."""

        logger.debug("inputs.shape: {}".format(inputs.shape))

        internals = inputs
        internals = internals - self.threshold
        internals = tf.where(
            internals < 0.0,
            x=(self.negative_slope * internals),
            y=(self.positive_slope * internals),
        )
        outputs = internals

        logger.debug("outputs.shape: {}".format(outputs.shape))

        return outputs

    def get_config(self):
        """Get configuration."""

        config = super().get_config()
        config.update({
            'shared_weights': self.shared_weights,
            'threshold_trainable': self.threshold_trainable,
            'negative_slope_trainable': self.negative_slope_trainable,
            'positive_slope_trainable': self.positive_slope_trainable,
        })

        return config
