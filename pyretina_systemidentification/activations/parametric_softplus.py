import logging
import tensorflow as tf


logger = logging.getLogger(__name__)


class ParametricSoftplusActivation(tf.keras.layers.Layer):
    """Parametric softplus.

    Applies the element-wise function:
    `softplus(x) = alpha * ln(1 + exp(beta * x + gamma))`
    Softplus is a smooth approximation to the ReLU function and can be used to constrain the output of a machine to
    always be positive.
    For numerical stability the implementation reverts to the linear function for inputs above a certain value.

    Arguments:
        threshold: float | none (optional)
            Output values above `alpha * threshold` revert to a linear function (for numerical stability).
            The default value is 20.0 (PyTorch default).
        minimum: none | float (optional)
            Output values below `minimum` revert to this value (to help learning with Poisson loss).
            The default value is None.
        weights_sharing_axes: none, 'all', tuple or list (optional)
            # TODO add description.
            The default value is 'all'.
        alpha_trainable: boolean (optional)
            # TODO add description.
            The default value is True.
        beta_trainable: boolean (optional)
            # TODO add description.
            The default value is True.
        gamma_trainable: boolean (optional)
            # TODO add description.
            The default value is True.
        name: string (optional)
            # TODO add description.
            The default value is 'parametric_softplus'.
        **kwargs
    """

    def __init__(
            self,
            threshold=20.0,
            minimum=None,
            weights_sharing_axes='all',
            alpha_initializer=1.0,
            beta_initializer=1.0,
            gamma_initializer=0.0,
            alpha_trainable=True,
            beta_trainable=True,
            gamma_trainable=True,
            name='parametric_softplus',
            **kwargs
    ):
        """Input-independent initialization of the layer."""

        if threshold is not None:
            assert threshold >= 10.0, threshold
        if minimum is not None:
            assert minimum >= 0.0, minimum

        super().__init__(name=name, **kwargs)

        self.threshold = threshold
        self.minimum = minimum
        self.weights_sharing_axes = weights_sharing_axes
        self.alpha_initializer = tf.keras.initializers.Constant(value=alpha_initializer)
        self.beta_initializer = tf.keras.initializers.Constant(value=beta_initializer)
        self.gamma_initializer = tf.keras.initializers.Constant(value=gamma_initializer)
        self.alpha_trainable = alpha_trainable
        self.beta_trainable = beta_trainable
        self.gamma_trainable = gamma_trainable

        # Declare weight variables.
        self.alpha = None
        self.beta = None
        self.gamma = None

        self._threshold = None
        self._minimum = None

    def build(self, input_shape):
        """Input-dependent initialization of the layer."""

        logger.debug("input_shape: {}".format(input_shape))

        # Resolve weight shape.
        # batch_size = input_shape[0]
        sample_shape = input_shape[1:]
        if self.weights_sharing_axes is None:
            shape = sample_shape
        elif self.weights_sharing_axes == 'all':
            shape = len(sample_shape) * (1,)
        elif isinstance(self.weights_sharing_axes, (tuple, list)):
            shape = list(sample_shape)
            for axis in self.weights_sharing_axes:
                shape[axis] = 1
            shape = tuple(shape)
        else:
            raise ValueError("unexpected value: {}".format(self.weights_sharing_axes))

        # Reshape threshold (if necessary).
        if self.threshold is not None:
            self._threshold = self.threshold * tf.ones(shape)
        # Reshape minimum (if necessary).
        if self.minimum is not None:
            self._minimum = self.minimum * tf.ones(shape)
        # Add alpha weight.
        self.alpha = self.add_weight(
            name='alpha',
            shape=shape,
            dtype=tf.float32,
            initializer=self.alpha_initializer,
            # regularizer=None,  # TODO add?
            trainable=self.alpha_trainable,
            constraint=tf.keras.constraints.NonNeg(),  # TODO check + add limits?
            # partitioner=None,
            # use_resource=None,
            # synchronization=tf.VariableSynchronization.AUTO,
            # aggregation=tf.compat.v1.VariableAggregation.NONE,
            # **kwargs
        )
        # Add beta weight.
        self.beta = self.add_weight(
            name='beta',
            shape=shape,
            dtype=tf.float32,
            initializer=self.beta_initializer,
            # regularizer=None,  # TODO add?
            trainable=self.beta_trainable,
            constraint=tf.keras.constraints.NonNeg(),  # TODO check + add limits?
            # partitioner=None,
            # use_resource=None,
            # synchronization=tf.VariableSynchronization.AUTO,
            # aggregation=tf.compat.v1.VariableAggregation.NONE,
            # **kwargs
        )
        # Add gamma weight.
        self.gamma = self.add_weight(
            name='gamma',
            shape=shape,
            dtype=tf.float32,
            initializer=self.gamma_initializer,
            # regularizer=None,  # TODO add?
            trainable=self.gamma_trainable,
            # constraint=None,  # TODO add limits?
            # partitioner=None,
            # use_resource=None,
            # synchronization=tf.VariableSynchronization.AUTO,
            # aggregation=tf.compat.v1.VariableAggregation.NONE,
            # **kwargs
        )

        return

    def call(self, inputs, **kwargs):
        """Forward computation of the layer."""

        # logger.debug("tf.shape(inputs): {}".format(tf.shape(inputs)))
        if self._threshold is None:
            internals = self.beta * inputs + self.gamma
            internals = tf.math.softplus(internals)
        else:
            # For numerical stability, reverts to the linear function for values above a certain threshold.
            internals_1 = self.beta * inputs + self.gamma
            internals_2 = tf.math.softplus(internals_1)
            condition = tf.math.greater(internals_2, self._threshold)
            internals = tf.where(condition, x=internals_1, y=internals_2)
            # logger.debug("internals: {}".format(internals))
        internals = self.alpha * internals
        if self._minimum is not None:
            internals = tf.math.maximum(self._minimum, internals)
            # internals = internals + self._minimum
        outputs = tf.identity(internals, name='output')  # TODO remove naming?
        # logger.debug("tf.shape(outputs): {}".format(tf.shape(outputs)))

        return outputs

    def get_config(self):
        """Get configuration."""

        config = super().get_config()
        config.update({
            'threshold': self.threshold,
            'minimum': self.minimum,
            'weights_sharing_axes': self.weights_sharing_axes,
            'alpha_trainable': self.alpha_trainable,
            'beta_trainable': self.beta_trainable,
            'gamma_trainable': self.gamma_trainable,
        })

        return config
