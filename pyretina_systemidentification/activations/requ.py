import logging

import tensorflow as tf


logger = logging.getLogger(__name__)


class ReQU(tf.keras.layers.Layer):
    """Rectified Quadratic Unit.

    `f(x) = 0  for x < 0`,
    `f(x) = x ** 2 for x >= 0`.

    Notes: c.f. usage in https://www.nature.com/articles/s41467-017-02159-y.
    """

    def __init__(self, name='ReQU', **kwargs):
        """Input-independent initialization of the layer."""

        super().__init__(name=name, **kwargs)

    # def build(self, input_shape):
    #     """Input-dependent initialization of the layer."""
    #
    #     # batch_size = input_shape[0]
    #     sample_shape = input_shape[1:]

    def call(self, inputs, **kwargs):
        """Forward computation of the layer."""

        internals = inputs
        internals = tf.math.maximum(internals, 0.0)  # i.e. ReLU
        internals = tf.math.square(internals)  # i.e. square
        outputs = internals

        return outputs

    # def get_config(self):
    #
    #     config = super().get_config()
    #
    #     return config
