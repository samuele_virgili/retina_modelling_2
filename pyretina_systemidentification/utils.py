import logging
import numpy as np


logger = logging.getLogger(__name__)


def corrcoef(x, y):
    """Return Pearson product-moment correlations coefficients.

    This is a wrapper around `np.corrcoef` to avoid:
        `RuntimeWarning: invalid value encountered in true_divide`.
    """

    assert len(x) > 0, len(x)
    assert len(y) > 0, len(y)
    assert len(x) == len(y), (len(x), len(y))

    is_x_deterministic = np.all(x == x[0])  # i.e. array filled with a unique value
    is_y_deterministic = np.all(y == y[0])  # i.e. array filled with a unique value
    if is_x_deterministic and is_y_deterministic:
        r = 1.0
    elif is_x_deterministic or is_y_deterministic:
        r = 0.0
    else:
        r = np.corrcoef(x, y)[0, 1]

    return r


def compute_sta(x, y):

    x = x[:, :, :, 0]  # i.e. drop channel dimension
    x_ = (x - np.mean(x)) / np.std(x)
    y_ = (y - np.mean(y, axis=0)) / np.std(y, axis=0)
    sta = np.tensordot(y_, x_, axes=([0], [0]))
    sta = sta[0, :, :]  # i.e. drop cell dimension

    logger.debug("x.shape: {}".format(x.shape))
    logger.debug("y.shape: {}".format(y.shape))
    logger.debug("sta.shape: {}".format(sta.shape))

    return sta


def deepupdate(dict_1, dict_2):

    for key, value in dict_2.items():
        if key not in dict_1:
            dict_1[key] = value
        else:
            if not isinstance(dict_1[key], dict):
                dict_1[key] = value
            else:
                dict_1[key] = deepupdate(dict_1[key], value)

    return dict_1


def plot_scale_bar(
        axis, scale=None, unit=None, loc='lower right', orientation='horizontal', with_label=True, color='black'
):

    pad = 0.06

    # fontsize = 'normal'
    # text_offset = 8  # pt
    fontsize = 'small'
    text_offset = 6  # pt

    x_min, x_max = axis.get_xlim()
    y_min, y_max = axis.get_ylim()

    # Find scale automatically (if necessary).
    if scale is None:
        width = (1.0 - 2.0 * pad) * (x_max - x_min)
        height = (1.0 - 2.0 * pad) * (y_max - y_min)
        if orientation == 'horizontal':
            size = width
        elif orientation == 'vertical':
            size = height
        else:
            raise ValueError("unexpected value: {}".format(orientation))
        order = int(np.floor(np.log10(size)))
        scale = 10 ** order
        factor = 1
    else:
        order = int(np.floor(np.log10(scale)))
        factor = scale / 10 ** order

    map_order = {
        -9: (1, 'n'),
        -8: (10, 'n'),
        -7: (100, 'n'),
        -6: (1, 'µ'),
        -5: (10, 'µ'),
        -4: (100, 'µ'),
        -3: (1, 'm'),
        -2: (10, 'm'),
        -1: (100, 'm'),
        0: (1, ''),
        1: (10, ''),
        2: (100, ''),
        3: (1, 'k'),
        4: (10, 'k'),
        5: (100, 'k'),
        6: (1, 'M'),
        7: (10, 'M'),
        8: (100, 'M'),
    }
    if unit is None:
        text = "{}".format(scale)
    else:
        factor_, prefix = map_order[order]
        factor = factor * factor_
        factor = int(factor) if factor.is_integer() else factor
        text = "{} {}{}".format(factor, prefix, unit)

    plot_kwargs = {
        'color': color,
        # 'linewidth': 3.5,
        'linewidth': 2.0,
        'solid_capstyle': 'butt',  # instead of 'projection (default)'
    }

    if loc == 'lower right':
        if orientation == 'horizontal':
            x_ref = x_max - pad * (x_max - x_min) - 0.5 * scale
            y_ref = y_min + pad * (y_max - y_min)
            axis.plot([x_ref - 0.5 * scale, x_ref + 0.5 * scale], [y_ref, y_ref], **plot_kwargs)
            if with_label:
                axis.annotate(
                    text, (x_ref, y_ref), xytext=(0, +text_offset),
                    xycoords='data', textcoords='offset points',
                    color=color, horizontalalignment='center', verticalalignment='center', rotation='horizontal',
                    fontsize=fontsize
                )
        elif orientation == 'vertical':
            x_ref = x_max - pad * (x_max - x_min)
            y_ref = y_min + pad * (y_max - y_min) + 0.5 * scale
            axis.plot([x_ref, x_ref], [y_ref - 0.5 * scale, y_ref + 0.5 * scale], **plot_kwargs)
            if with_label:
                axis.annotate(
                    text, (x_ref, y_ref), xytext=(-text_offset, 0),
                    xycoords='data', textcoords='offset points',
                    color=color, horizontalalignment='center', verticalalignment='center', rotation='vertical',
                    fontsize=fontsize
                )
        else:
            raise ValueError("unexpected value: {}".format(orientation))
    elif loc == 'lower left':
        if orientation == 'horizontal':
            # TODO remove the following lines?
            # x_ref = x_min + pad * (x_max - x_min) + 0.5 * scale
            # y_ref = y_min + pad * (y_max - y_min)
            # axis.plot([x_ref - 0.5 * scale, x_ref + 0.5 * scale], [y_ref, y_ref], **plot_kwargs)
            # if with_label:
            #     axis.annotate(
            #         text, (x_ref, y_ref), xytext=(0, 8),
            #         xycoords='data', textcoords='offset points',
            #         horizontalalignment='center', verticalalignment='center', rotation='horizontal',
            #     )
            x_ref = x_min + pad * (x_max - x_min)
            y_ref = y_min + pad * (y_max - y_min)
            axis.plot([x_ref + 0.0 * scale, x_ref + 1.0 * scale], [y_ref, y_ref], **plot_kwargs)
            if with_label:
                axis.annotate(
                    text, (x_ref, y_ref), xytext=(0, +text_offset),
                    xycoords='data', textcoords='offset points',
                    color=color, horizontalalignment='left', verticalalignment='center', rotation='horizontal',
                    fontsize=fontsize
                )
        elif orientation == 'vertical':
            x_ref = x_min + pad * (x_max - x_min)
            y_ref = y_min + pad * (y_max - y_min) + 0.5 * scale
            axis.plot([x_ref, x_ref], [y_ref - 0.5 * scale, y_ref + 0.5 * scale], **plot_kwargs)
            if with_label:
                axis.annotate(
                    text, (x_ref, y_ref), xytext=(+text_offset, 0),
                    xycoords='data', textcoords='offset points',
                    color=color, horizontalalignment='center', verticalalignment='center', rotation='vertical',
                    fontsize=fontsize
                )
        else:
            raise ValueError("unexpected value: {}".format(orientation))
    else:
        raise NotImplementedError()

    return
