import logging
import tensorflow as tf

from tensorflow.python.ops import math_ops


logger = logging.getLogger(__name__)


class L1Smooth2DRegularizer(tf.keras.regularizers.Regularizer):

    def __init__(self, sparsity_factor=0.001, smoothness_factor=0.001):

        super().__init__()

        if sparsity_factor:
            # self.sparsity_factor = tf.cast(sparsity_factor, tf.float32, name=None)  # TODO remove!
            self.sparsity_factor = sparsity_factor
        else:
            self.sparsity_factor = None

        if smoothness_factor:
            # self.smoothness_factor = tf.cast(smoothness_factor, tf.float32, name=None)  # TODO remove!
            self.smoothness_factor = smoothness_factor
        else:
            self.smoothness_factor = None

        self.padding = 'constant'  # /!\ edge weights will tend to zeroz
        # self.padding = 'symmetric'  # /!\ edge weights not penalized correctly

    def __call__(self, x):  # TODO rename `x` to `weights`?

        regularization = tf.constant(0.0, dtype=tf.float32, shape=None, name=None)

        # Add L1 regularization (if necessary).
        if self.sparsity_factor:
            sparsity_regularization = math_ops.reduce_sum(math_ops.abs(x))
            regularization += self.sparsity_factor * sparsity_regularization

        # Add smoothness regularization (if necessary).
        if self.smoothness_factor:
            # lap = tf.constant([
            #     [+0.25, +0.50, +0.25],
            #     [+0.50, -3.00, +0.50],
            #     [+0.25, +0.50, +0.25],
            # ])
            # lap = tf.expand_dims(lap, tf.expand_dims(lap, 2), 3, name='laplacian_filter')
            lap = tf.constant([
                [+0.25, +0.50, +0.25],
                [+0.50, -3.00, +0.50],
                [+0.25, +0.50, +0.25],
            ], shape=(3, 3, 1, 1), name='laplacian_filter')
            logger.debug("lap.shape: {}".format(lap.shape))
            # nb_kernels = x.get_shape().as_list()[2]
            # nb_kernels = x.shape[2]
            # _, _, nb_kernels, _ = x.shape  # TODO remove (incorrect)?
            logger.debug("x.shape: {}".format(x.shape))
            # height, width, nb_channels, nb_kernels = x.shape  # TODO invert last two?
            paddings = tf.constant([
                [1, 1],  # i.e. add one value before and one value after content along the height axis
                [1, 1],  # i.e. add one value before and one value after content along the width axis
                [0, 0],
                [0, 0],
            ])
            if self.padding == 'constant':
                x_pad = tf.pad(
                    x,
                    paddings,
                    # mode='CONSTANT',
                    # constant_values=0.0,
                    # name=None,
                )
            elif self.padding == 'symmetric':
                x_pad = tf.pad(
                    x,
                    paddings,
                    mode='SYMMETRIC',
                    # name=None,
                )
            else:
                raise ValueError("unexpected value: {}".format(self.padding))
            logger.debug("x_pad.shape: {}".format(x_pad.shape))
            height, width, nb_channels, nb_kernels = x_pad.shape
            # x_bis = tf.transpose(x, perm=(3, 0, 1, 2))  # TODO remove?
            x_bis = tf.transpose(x_pad, perm=(3, 0, 1, 2))
            logger.debug("x_bis.shape: {}".format(x_bis.shape))
            # nb_kernels, height, width, nb_channels = x_bis.shape
            multiples = (1, 1, nb_channels, 1)
            filter_ = tf.tile(
                lap,
                multiples,
                # name=None,
            )
            # _, _, nb_filters, _ = filter_.shape
            logger.debug("filter_.shape: {}".format(filter_.shape))
            if self.padding == 'constant':
                x_lap = tf.nn.depthwise_conv2d(
                    x_bis,  # i.e. inputs
                    filter_,  # i.e. filter
                    (1, 1, 1, 1),  # i.e. strides
                    'SAME',  # i.e. padding
                )
            elif self.padding == 'symmetric':
                x_lap = tf.nn.depthwise_conv2d(
                    x_bis,  # i.e. inputs
                    filter_,  # i.e. filter
                    (1, 1, 1, 1),  # i.e. strides
                    'VALID',  # i.e. padding
                )
            else:
                raise ValueError("unexpected value: {}".format(self.padding))
            logger.debug("x_lap.shape: {}".format(x_lap))
            # nb_kernels, height, width, nb_channels = x_lap.shape
            tmp_1 = math_ops.reduce_sum(math_ops.square(x_lap), axis=(1, 2, 3))
            logger.debug("tmp_1: {}".format(tmp_1))
            tmp_2 = (1e-8 + math_ops.reduce_sum(math_ops.square(x), axis=(0, 1, 2)))
            logger.debug("tmp_2: {}".format(tmp_2))
            smoothness_regularization = math_ops.reduce_sum(
                tmp_1 / tmp_2
            )
            logger.debug("smoothness_regularization: {}".format(smoothness_regularization))
            tmp_3 = math_ops.sqrt(math_ops.reduce_sum(math_ops.square(x_lap)))
            logger.debug("tmp_3: {}".format(tmp_3))
            # regularization += self.smoothness_factor * smoothness_regularization  # TODO remove?
            regularization += self.smoothness_factor * tmp_3

        regularization = tf.identity(regularization, name='regularization')  # TODO remove?

        return regularization

    def get_config(self):

        # return {
        #     'smooth_factor': self.smooth_factor,
        #     'sparse_factor': self.sparse_factor,
        # }

        raise NotImplementedError("TODO")  # TODO complete!


# TODO is it possible to combine regularizers instead?
