import argparse
import datetime
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import warnings

# from pyretina_systemidentification.models.regular_cnn import create_regular_cnn_model
# from pyretina_systemidentification.models.regular_cnn import create_klindt_cnn_model
from pyretina_systemidentification.models.regular_cnn import create_ecker_cnn_model
from data import Dataset


__basename__ = os.path.basename(__file__)
__name__, _ = os.path.splitext(__basename__)
__time__ = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")


# Parse input arguments.
parser = argparse.ArgumentParser(description='baseline')
parser.add_argument(
    '-ei', '--experiment-identifier',
    nargs='?', type=str, const=None, default=__time__
)
# parser.add_argument(
#     '-cn', '--cell-number',
#     nargs='?', type=int, const=None, default=None
# )
parser.add_argument(
    '-rn', '--run-number',
    nargs='?', type=int, const=None, default=None
)
args = parser.parse_args()


# Set experiment identifier.
experiment_identifier = args.experiment_identifier
assert experiment_identifier is not None  # TODO correct?


# cell_nb = args.cell_number
# assert cell_nb is not None  # TODO correct?


run_nb = args.run_number


# Set up experiment directory.
experiment_directory = "{}".format(experiment_identifier)
assert os.path.isdir(experiment_directory), experiment_directory


# Set up run directory.
run_directory = os.path.join(experiment_directory, "run_{:03d}".format(run_nb))
assert os.path.isdir(run_directory), run_directory


# Set up logging.
log_filename = "{}_{}.log".format(__name__, __time__)
log_path = os.path.join(experiment_directory, log_filename)
log_level = logging.DEBUG
# #
logging.basicConfig(
    filename=log_path,
    level=log_level,
)
# # Prevent Matplotlib from using the same log level.
matplotlib_logger = logging.getLogger(name='matplotlib')
matplotlib_logger.setLevel(logging.INFO)
# # Get logger.
logger = logging.getLogger(name=__name__)


# Load test observations.
test_observations_filename = "test_observations.npy"
test_observations_path = os.path.join(run_directory, test_observations_filename)
test_observations = np.load(test_observations_path)
# nb_repetitions, nb_images, nb_cells = test_observations.shape

# Load test predictions.
test_predictions_filename = "test_predictions.npy"
test_predictions_path = os.path.join(run_directory, test_predictions_filename)
test_predictions = np.load(test_predictions_path)
# nb_repetitions, nb_images, nb_cells = test_predictions.shape


assert test_observations.shape == test_predictions.shape, (test_observations.shape, test_predictions.shape)
nb_repetitions, nb_images, nb_cells = test_observations.shape


metrics = {}


def corrcoef(x, y):
    """Return Pearson product-moment correlations coefficients.

    This is a wrapper around `np.corrcoef` to avoid:
        `RuntimeWarning: invalid value encountered in true_divide`.
    """

    assert len(x) > 0, len(x)
    assert len(y) > 0, len(y)
    assert len(x) == len(y), (len(x), len(y))

    is_x_deterministic = np.all(x == x[0])  # i.e. array filled with a unique value
    is_y_deterministic = np.all(y == y[0])  # i.e. array filled with a unique value
    if is_x_deterministic and is_y_deterministic:
        r = 1.0
    elif is_x_deterministic or is_y_deterministic:
        r = 0.0
    else:
        r = np.corrcoef(x, y)[0, 1]

    return r


# TODO compute Pearson correlation.
r = []
for cell_nb in range(0, nb_cells):
    x = np.mean(test_observations[:, :, cell_nb], axis=0)
    y = np.mean(test_predictions[:, :, cell_nb], axis=0)
    r.append(corrcoef(x, y))
r = np.array(r)
metrics['r'] = r


# TODO compute R-squared value.
R2 = np.square(r)
metrics['R2'] = R2


# TODO compute reliability.
reliability = []
for cell_nb in range(0, nb_cells):
    x = np.mean(test_observations[0::2, :, cell_nb], axis=0)
    y = np.mean(test_observations[1::2, :, cell_nb], axis=0)
    reliability.append(corrcoef(x, y))
reliability = np.array(reliability)
metrics['reliability'] = reliability


# TODO compute r_even.
r_even = []
for cell_nb in range(0, nb_cells):
    x = np.mean(test_observations[0::2, :, cell_nb], axis=0)
    y = np.mean(test_predictions[0::2, :, cell_nb], axis=0)
    r_even.append(corrcoef(x, y))
r_even = np.array(r_even)
metrics['r_even'] = r_even


# TODO compute r_odd.
r_odd = []
for cell_nb in range(0, nb_cells):
    x = np.mean(test_observations[1::2, :, cell_nb], axis=0)
    y = np.mean(test_predictions[1::2, :, cell_nb], axis=0)
    r_odd.append(corrcoef(x, y))
r_odd = np.array(r_odd)
metrics['r_odd'] = r_odd
# metrics['accuracy'] = r_odd  # for backward compatibility


# TODO compute noise corrected Pearson correlation.
r_c = 0.5 * (r_even + r_odd) / np.sqrt(reliability)
metrics['r_c'] = r_c


# TODO compute noise corrected R-squared value.
R2_c = np.square(r_c)
metrics['R2_c'] = R2_c


# TODO compute upper bound.
assert np.all(reliability >= 0.0), reliability
# R2_ub = 1.0 / (0.5 * nb_repetitions * (np.sqrt((1.0 / np.square(reliability)) - 1.0)) + 1.0)  # c.f. (Hu et al., 2004)
R2_ub = 1.0 / (0.5 * 1.0 * (np.sqrt((1.0 / np.square(reliability)) - 1.0)) + 1.0)  # c.f. (Schoppe et al., 2016)
metrics['R2_ub'] = R2_ub


# TODO compute fraction of variance unexplained.
FVU = 1 - R2_c
metrics['FVU'] = FVU


# TODO compute fraction of explainable variance explained.
FEV = []
V_n = np.mean(np.var(test_observations, axis=0), axis=0)
MSE = np.mean(np.square(test_observations - test_predictions), axis=(0, 1))
V = np.var(test_observations, axis=(0, 1))
FEV = 1 - (MSE - V_n) / (V - V_n)
# FEV[FEV < 0.0] = 0.0  # i.e. mean of data provides a better fit
metrics['V_n'] = V_n
metrics['MSE'] = MSE
metrics['V'] = V
metrics['FEV'] = FEV


# TODO complete?


# # TODO compute deviance.
# deviance = []
# for cell_nb in range(0, nb_cells):
#     x = test_observations[:, :, cell_nb]
#     y = test_predictions[:, :, cell_nb]
#     # x = np.mean(x, axis=0)
#     # y = np.mean(y, axis=0)
#     with warnings.catch_warnings():
#         warnings.filterwarnings('error')
#         try:
#             d = np.mean(2.0 * (x * np.log(x / y) - x + y))
#         except RuntimeWarning:
#             raise NotImplementedError()  # TODO find a solution?
#     deviance.append()
# deviance = np.array(deviance)
# metrics['deviance'] = deviance


# TODO save metrics to csv.
df = pd.DataFrame.from_dict(metrics)
output_filename = "evaluation_metrics.csv"
output_path = os.path.join(run_directory, output_filename)
df.to_csv(output_path, index=True, index_label="cell_nb")
print("{} saved.".format(output_path))


# TODO print mean R2_ub.
print("mean R-squared upper bound: {} (+/- {})".format(np.mean(R2_ub), np.std(R2_ub) / np.sqrt(nb_cells)))
# TODO print mean R2.
print("mean R-squared: {} (+/- {})".format(np.mean(R2), np.std(R2) / np.sqrt(nb_cells)))
# TODO print mean R2_c.
print("mean noise corrected R-squared: {} (+/- {})".format(np.mean(R2_c), np.std(R2_c) / np.sqrt(nb_cells)))


# # TODO plot R2.
# subplots_kwargs = {
#     'figsize': (5.0, 2.0),
# }
# fig, ax = plt.subplots(**subplots_kwargs)
# x = np.arange(0, nb_cells)
# height = R2
# bar_kwargs = {
#     'width': 0.8,
#     'align': 'center',
#     'color': 'black',
# }
# ax.bar(x, height, **bar_kwargs)
# ax.set_xlim(0.0 - 0.5, (nb_cells - 1) + 0.5)
# ax.set_ylim(0.0, 1.0)
# ax.set_xlabel("cell")
# ax.set_ylabel("$R^2$")
# fig.tight_layout()


# # TODO plot r_odd vs r_even.
# fig, ax = plt.subplots()
# ax.set_aspect('equal')
# ax.plot([-1, +1], [-1, +1], color='grey')
# ax.scatter(r_even, r_odd, color='black')
# ax.set_xlabel("$r_{even}$")
# ax.set_ylabel("$r_{odd}$")
# fig.tight_layout()
# # NB: Not too much of a difference.


# # TODO plot R2_ub and R2_c.
subplots_kwargs = {
    'nrows': 2,
    'squeeze': False,
    'figsize': (5.0, 3.0),
}
fig, axes = plt.subplots(**subplots_kwargs)
# # 1st plot.
ax = axes[0, 0]
x = np.arange(0, nb_cells)
height = R2_ub
bar_kwargs = {
    'width': 0.8,
    'align': 'center',
    'color': 'grey',
    'label': 'upper bound',
}
ax.bar(x, height, **bar_kwargs)
height = R2_c * R2_ub
bar_kwargs = {
    'width': 0.6,
    'align': 'center',
    'color': 'black',
    'label': 'CNN',
}
ax.bar(x, height, **bar_kwargs)
ax.set_xlim(0.0 - 0.5, (nb_cells - 1) + 0.5)
ax.set_ylim(0.0, 1.0)
ax.set_xlabel("cell")
ax.set_ylabel("$R^2$")
ax.legend()
# # 2nd plot.
ax = axes[1, 0]
x = np.arange(0, nb_cells)
height = R2_c
bar_kwargs = {
    'width': 0.8,
    'align': 'center',
    'color': 'black',
    'label': 'CNN',
}
ax.bar(x, height, **bar_kwargs)
ax.set_xlim(0.0 - 0.5, (nb_cells - 1) + 0.5)
ax.set_ylim(0.0, 1.0)
ax.set_xlabel("cell")
ax.set_ylabel("$R^2_c$")
ax.legend()
fig.tight_layout()


# TODO plot R2 vs R2_ub.
fig, ax = plt.subplots()
ax.set_aspect('equal')
# v_min = -1
v_min = 0
ax.plot([v_min, +1], [v_min, +1], color='grey')
ax.plot([v_min, +1], [0, 0], color='grey')
ax.plot([0, 0], [v_min, +1], color='grey')
ax.scatter(R2_ub, R2, color='black')
ax.set_xlabel("$R^2_{ub}$")
ax.set_ylabel("$R^2$")
fig.tight_layout()


# TODO plot R2_c vs R2.
fig, ax = plt.subplots()
ax.set_aspect('equal')
ax.plot([-1, +1], [-1, +1], color='grey')
ax.scatter(R2, R2_c, color='black')
ax.set_xlabel("$R^2$")
ax.set_ylabel("$R^2_c$")
fig.tight_layout()
# NB: Not too much of a difference.


# TODO plot FEV vs R2_c.
fig, ax = plt.subplots()
ax.set_aspect('equal')
ax.plot([-1, +1], [-1, +1], color='grey')
ax.scatter(R2_c, FEV, color='black')
ax.set_xlabel("$R^2_c$")
ax.set_ylabel("$FEV$")
fig.tight_layout()
# NB: Not too much of a difference.


# TODO show figures (if necessary).
plt.show() if plt.get_fignums() else None
